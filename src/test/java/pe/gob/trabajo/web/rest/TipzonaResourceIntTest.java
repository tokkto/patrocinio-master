package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Tipzona;
import pe.gob.trabajo.repository.TipzonaRepository;
import pe.gob.trabajo.repository.search.TipzonaSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipzonaResource REST controller.
 *
 * @see TipzonaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class TipzonaResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_DESCCORTA = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCCORTA = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipzonaRepository tipzonaRepository;

    @Autowired
    private TipzonaSearchRepository tipzonaSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipzonaMockMvc;

    private Tipzona tipzona;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipzonaResource tipzonaResource = new TipzonaResource(tipzonaRepository, tipzonaSearchRepository);
        this.restTipzonaMockMvc = MockMvcBuilders.standaloneSetup(tipzonaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipzona createEntity(EntityManager em) {
        Tipzona tipzona = new Tipzona()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vDesccorta(DEFAULT_V_DESCCORTA)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipzona;
    }

    @Before
    public void initTest() {
        tipzonaSearchRepository.deleteAll();
        tipzona = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipzona() throws Exception {
        int databaseSizeBeforeCreate = tipzonaRepository.findAll().size();

        // Create the Tipzona
        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isCreated());

        // Validate the Tipzona in the database
        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeCreate + 1);
        Tipzona testTipzona = tipzonaList.get(tipzonaList.size() - 1);
        assertThat(testTipzona.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipzona.getvDesccorta()).isEqualTo(DEFAULT_V_DESCCORTA);
        assertThat(testTipzona.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testTipzona.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipzona.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipzona.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipzona.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testTipzona.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipzona.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipzona in Elasticsearch
        Tipzona tipzonaEs = tipzonaSearchRepository.findOne(testTipzona.getId());
        assertThat(tipzonaEs).isEqualToComparingFieldByField(testTipzona);
    }

    @Test
    @Transactional
    public void createTipzonaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipzonaRepository.findAll().size();

        // Create the Tipzona with an existing ID
        tipzona.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isBadRequest());

        // Validate the Tipzona in the database
        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipzonaRepository.findAll().size();
        // set the field null
        tipzona.setvDescrip(null);

        // Create the Tipzona, which fails.

        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isBadRequest());

        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesccortaIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipzonaRepository.findAll().size();
        // set the field null
        tipzona.setvDesccorta(null);

        // Create the Tipzona, which fails.

        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isBadRequest());

        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipzonaRepository.findAll().size();
        // set the field null
        tipzona.setvUsuareg(null);

        // Create the Tipzona, which fails.

        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isBadRequest());

        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipzonaRepository.findAll().size();
        // set the field null
        tipzona.settFecreg(null);

        // Create the Tipzona, which fails.

        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isBadRequest());

        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipzonaRepository.findAll().size();
        // set the field null
        tipzona.setnSedereg(null);

        // Create the Tipzona, which fails.

        restTipzonaMockMvc.perform(post("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isBadRequest());

        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipzonas() throws Exception {
        // Initialize the database
        tipzonaRepository.saveAndFlush(tipzona);

        // Get all the tipzonaList
        restTipzonaMockMvc.perform(get("/api/tipzonas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipzona.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vDesccorta").value(hasItem(DEFAULT_V_DESCCORTA.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipzona() throws Exception {
        // Initialize the database
        tipzonaRepository.saveAndFlush(tipzona);

        // Get the tipzona
        restTipzonaMockMvc.perform(get("/api/tipzonas/{id}", tipzona.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipzona.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vDesccorta").value(DEFAULT_V_DESCCORTA.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipzona() throws Exception {
        // Get the tipzona
        restTipzonaMockMvc.perform(get("/api/tipzonas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipzona() throws Exception {
        // Initialize the database
        tipzonaRepository.saveAndFlush(tipzona);
        tipzonaSearchRepository.save(tipzona);
        int databaseSizeBeforeUpdate = tipzonaRepository.findAll().size();

        // Update the tipzona
        Tipzona updatedTipzona = tipzonaRepository.findOne(tipzona.getId());
        updatedTipzona
            .vDescrip(UPDATED_V_DESCRIP)
            .vDesccorta(UPDATED_V_DESCCORTA)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipzonaMockMvc.perform(put("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipzona)))
            .andExpect(status().isOk());

        // Validate the Tipzona in the database
        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeUpdate);
        Tipzona testTipzona = tipzonaList.get(tipzonaList.size() - 1);
        assertThat(testTipzona.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipzona.getvDesccorta()).isEqualTo(UPDATED_V_DESCCORTA);
        assertThat(testTipzona.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testTipzona.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipzona.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipzona.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipzona.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testTipzona.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipzona.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipzona in Elasticsearch
        Tipzona tipzonaEs = tipzonaSearchRepository.findOne(testTipzona.getId());
        assertThat(tipzonaEs).isEqualToComparingFieldByField(testTipzona);
    }

    @Test
    @Transactional
    public void updateNonExistingTipzona() throws Exception {
        int databaseSizeBeforeUpdate = tipzonaRepository.findAll().size();

        // Create the Tipzona

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipzonaMockMvc.perform(put("/api/tipzonas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipzona)))
            .andExpect(status().isCreated());

        // Validate the Tipzona in the database
        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipzona() throws Exception {
        // Initialize the database
        tipzonaRepository.saveAndFlush(tipzona);
        tipzonaSearchRepository.save(tipzona);
        int databaseSizeBeforeDelete = tipzonaRepository.findAll().size();

        // Get the tipzona
        restTipzonaMockMvc.perform(delete("/api/tipzonas/{id}", tipzona.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipzonaExistsInEs = tipzonaSearchRepository.exists(tipzona.getId());
        assertThat(tipzonaExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipzona> tipzonaList = tipzonaRepository.findAll();
        assertThat(tipzonaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipzona() throws Exception {
        // Initialize the database
        tipzonaRepository.saveAndFlush(tipzona);
        tipzonaSearchRepository.save(tipzona);

        // Search the tipzona
        restTipzonaMockMvc.perform(get("/api/_search/tipzonas?query=id:" + tipzona.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipzona.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vDesccorta").value(hasItem(DEFAULT_V_DESCCORTA.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipzona.class);
        Tipzona tipzona1 = new Tipzona();
        tipzona1.setId(1L);
        Tipzona tipzona2 = new Tipzona();
        tipzona2.setId(tipzona1.getId());
        assertThat(tipzona1).isEqualTo(tipzona2);
        tipzona2.setId(2L);
        assertThat(tipzona1).isNotEqualTo(tipzona2);
        tipzona1.setId(null);
        assertThat(tipzona1).isNotEqualTo(tipzona2);
    }
}
