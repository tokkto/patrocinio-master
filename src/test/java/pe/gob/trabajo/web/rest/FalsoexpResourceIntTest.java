package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Falsoexp;
import pe.gob.trabajo.repository.FalsoexpRepository;
import pe.gob.trabajo.repository.search.FalsoexpSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FalsoexpResource REST controller.
 *
 * @see FalsoexpResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class FalsoexpResourceIntTest {

    private static final String DEFAULT_V_CODREG = "AAAAA";
    private static final String UPDATED_V_CODREG = "BBBBB";

    private static final String DEFAULT_V_CARGOFAL = "AAAAAAAAAA";
    private static final String UPDATED_V_CARGOFAL = "BBBBBBBBBB";

    private static final String DEFAULT_V_DEMNDTE = "AAAAAAAAAA";
    private static final String UPDATED_V_DEMNDTE = "BBBBBBBBBB";

    private static final String DEFAULT_V_DEMANDADO = "AAAAAAAAAA";
    private static final String UPDATED_V_DEMANDADO = "BBBBBBBBBB";

    private static final String DEFAULT_V_DNIDMDO = "AAAAAAAAAA";
    private static final String UPDATED_V_DNIDMDO = "BBBBBBBBBB";

    private static final String DEFAULT_V_DNIDMTE = "AAAAAAAAAA";
    private static final String UPDATED_V_DNIDMTE = "BBBBBBBBBB";

    private static final String DEFAULT_V_NOMBRES = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMBRES = "BBBBBBBBBB";

    private static final String DEFAULT_V_DIFFAL = "AAAA";
    private static final String UPDATED_V_DIFFAL = "BBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private FalsoexpRepository falsoexpRepository;

    @Autowired
    private FalsoexpSearchRepository falsoexpSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFalsoexpMockMvc;

    private Falsoexp falsoexp;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FalsoexpResource falsoexpResource = new FalsoexpResource(falsoexpRepository, falsoexpSearchRepository);
        this.restFalsoexpMockMvc = MockMvcBuilders.standaloneSetup(falsoexpResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Falsoexp createEntity(EntityManager em) {
        Falsoexp falsoexp = new Falsoexp()
            .vCodreg(DEFAULT_V_CODREG)
            .vCargofal(DEFAULT_V_CARGOFAL)
            .vDemndte(DEFAULT_V_DEMNDTE)
            .vDemandado(DEFAULT_V_DEMANDADO)
            .vDnidmdo(DEFAULT_V_DNIDMDO)
            .vDnidmte(DEFAULT_V_DNIDMTE)
            .vNombres(DEFAULT_V_NOMBRES)
            .vDiffal(DEFAULT_V_DIFFAL)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return falsoexp;
    }

    @Before
    public void initTest() {
        falsoexpSearchRepository.deleteAll();
        falsoexp = createEntity(em);
    }

    @Test
    @Transactional
    public void createFalsoexp() throws Exception {
        int databaseSizeBeforeCreate = falsoexpRepository.findAll().size();

        // Create the Falsoexp
        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isCreated());

        // Validate the Falsoexp in the database
        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeCreate + 1);
        Falsoexp testFalsoexp = falsoexpList.get(falsoexpList.size() - 1);
        assertThat(testFalsoexp.getvCodreg()).isEqualTo(DEFAULT_V_CODREG);
        assertThat(testFalsoexp.getvCargofal()).isEqualTo(DEFAULT_V_CARGOFAL);
        assertThat(testFalsoexp.getvDemndte()).isEqualTo(DEFAULT_V_DEMNDTE);
        assertThat(testFalsoexp.getvDemandado()).isEqualTo(DEFAULT_V_DEMANDADO);
        assertThat(testFalsoexp.getvDnidmdo()).isEqualTo(DEFAULT_V_DNIDMDO);
        assertThat(testFalsoexp.getvDnidmte()).isEqualTo(DEFAULT_V_DNIDMTE);
        assertThat(testFalsoexp.getvNombres()).isEqualTo(DEFAULT_V_NOMBRES);
        assertThat(testFalsoexp.getvDiffal()).isEqualTo(DEFAULT_V_DIFFAL);
        assertThat(testFalsoexp.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testFalsoexp.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testFalsoexp.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testFalsoexp.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testFalsoexp.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testFalsoexp.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testFalsoexp.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Falsoexp in Elasticsearch
        Falsoexp falsoexpEs = falsoexpSearchRepository.findOne(testFalsoexp.getId());
        assertThat(falsoexpEs).isEqualToComparingFieldByField(testFalsoexp);
    }

    @Test
    @Transactional
    public void createFalsoexpWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = falsoexpRepository.findAll().size();

        // Create the Falsoexp with an existing ID
        falsoexp.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        // Validate the Falsoexp in the database
        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodregIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvCodreg(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCargofalIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvCargofal(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDemndteIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvDemndte(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDemandadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvDemandado(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDnidmdoIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvDnidmdo(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDnidmteIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvDnidmte(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvNombresIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvNombres(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDiffalIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setvDiffal(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setnUsuareg(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.settFecreg(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setnFlgactivo(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = falsoexpRepository.findAll().size();
        // set the field null
        falsoexp.setnSedereg(null);

        // Create the Falsoexp, which fails.

        restFalsoexpMockMvc.perform(post("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isBadRequest());

        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFalsoexps() throws Exception {
        // Initialize the database
        falsoexpRepository.saveAndFlush(falsoexp);

        // Get all the falsoexpList
        restFalsoexpMockMvc.perform(get("/api/falsoexps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(falsoexp.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCargofal").value(hasItem(DEFAULT_V_CARGOFAL.toString())))
            .andExpect(jsonPath("$.[*].vDemndte").value(hasItem(DEFAULT_V_DEMNDTE.toString())))
            .andExpect(jsonPath("$.[*].vDemandado").value(hasItem(DEFAULT_V_DEMANDADO.toString())))
            .andExpect(jsonPath("$.[*].vDnidmdo").value(hasItem(DEFAULT_V_DNIDMDO.toString())))
            .andExpect(jsonPath("$.[*].vDnidmte").value(hasItem(DEFAULT_V_DNIDMTE.toString())))
            .andExpect(jsonPath("$.[*].vNombres").value(hasItem(DEFAULT_V_NOMBRES.toString())))
            .andExpect(jsonPath("$.[*].vDiffal").value(hasItem(DEFAULT_V_DIFFAL.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getFalsoexp() throws Exception {
        // Initialize the database
        falsoexpRepository.saveAndFlush(falsoexp);

        // Get the falsoexp
        restFalsoexpMockMvc.perform(get("/api/falsoexps/{id}", falsoexp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(falsoexp.getId().intValue()))
            .andExpect(jsonPath("$.vCodreg").value(DEFAULT_V_CODREG.toString()))
            .andExpect(jsonPath("$.vCargofal").value(DEFAULT_V_CARGOFAL.toString()))
            .andExpect(jsonPath("$.vDemndte").value(DEFAULT_V_DEMNDTE.toString()))
            .andExpect(jsonPath("$.vDemandado").value(DEFAULT_V_DEMANDADO.toString()))
            .andExpect(jsonPath("$.vDnidmdo").value(DEFAULT_V_DNIDMDO.toString()))
            .andExpect(jsonPath("$.vDnidmte").value(DEFAULT_V_DNIDMTE.toString()))
            .andExpect(jsonPath("$.vNombres").value(DEFAULT_V_NOMBRES.toString()))
            .andExpect(jsonPath("$.vDiffal").value(DEFAULT_V_DIFFAL.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingFalsoexp() throws Exception {
        // Get the falsoexp
        restFalsoexpMockMvc.perform(get("/api/falsoexps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFalsoexp() throws Exception {
        // Initialize the database
        falsoexpRepository.saveAndFlush(falsoexp);
        falsoexpSearchRepository.save(falsoexp);
        int databaseSizeBeforeUpdate = falsoexpRepository.findAll().size();

        // Update the falsoexp
        Falsoexp updatedFalsoexp = falsoexpRepository.findOne(falsoexp.getId());
        updatedFalsoexp
            .vCodreg(UPDATED_V_CODREG)
            .vCargofal(UPDATED_V_CARGOFAL)
            .vDemndte(UPDATED_V_DEMNDTE)
            .vDemandado(UPDATED_V_DEMANDADO)
            .vDnidmdo(UPDATED_V_DNIDMDO)
            .vDnidmte(UPDATED_V_DNIDMTE)
            .vNombres(UPDATED_V_NOMBRES)
            .vDiffal(UPDATED_V_DIFFAL)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restFalsoexpMockMvc.perform(put("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFalsoexp)))
            .andExpect(status().isOk());

        // Validate the Falsoexp in the database
        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeUpdate);
        Falsoexp testFalsoexp = falsoexpList.get(falsoexpList.size() - 1);
        assertThat(testFalsoexp.getvCodreg()).isEqualTo(UPDATED_V_CODREG);
        assertThat(testFalsoexp.getvCargofal()).isEqualTo(UPDATED_V_CARGOFAL);
        assertThat(testFalsoexp.getvDemndte()).isEqualTo(UPDATED_V_DEMNDTE);
        assertThat(testFalsoexp.getvDemandado()).isEqualTo(UPDATED_V_DEMANDADO);
        assertThat(testFalsoexp.getvDnidmdo()).isEqualTo(UPDATED_V_DNIDMDO);
        assertThat(testFalsoexp.getvDnidmte()).isEqualTo(UPDATED_V_DNIDMTE);
        assertThat(testFalsoexp.getvNombres()).isEqualTo(UPDATED_V_NOMBRES);
        assertThat(testFalsoexp.getvDiffal()).isEqualTo(UPDATED_V_DIFFAL);
        assertThat(testFalsoexp.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testFalsoexp.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testFalsoexp.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testFalsoexp.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testFalsoexp.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testFalsoexp.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testFalsoexp.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Falsoexp in Elasticsearch
        Falsoexp falsoexpEs = falsoexpSearchRepository.findOne(testFalsoexp.getId());
        assertThat(falsoexpEs).isEqualToComparingFieldByField(testFalsoexp);
    }

    @Test
    @Transactional
    public void updateNonExistingFalsoexp() throws Exception {
        int databaseSizeBeforeUpdate = falsoexpRepository.findAll().size();

        // Create the Falsoexp

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFalsoexpMockMvc.perform(put("/api/falsoexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(falsoexp)))
            .andExpect(status().isCreated());

        // Validate the Falsoexp in the database
        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFalsoexp() throws Exception {
        // Initialize the database
        falsoexpRepository.saveAndFlush(falsoexp);
        falsoexpSearchRepository.save(falsoexp);
        int databaseSizeBeforeDelete = falsoexpRepository.findAll().size();

        // Get the falsoexp
        restFalsoexpMockMvc.perform(delete("/api/falsoexps/{id}", falsoexp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean falsoexpExistsInEs = falsoexpSearchRepository.exists(falsoexp.getId());
        assertThat(falsoexpExistsInEs).isFalse();

        // Validate the database is empty
        List<Falsoexp> falsoexpList = falsoexpRepository.findAll();
        assertThat(falsoexpList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchFalsoexp() throws Exception {
        // Initialize the database
        falsoexpRepository.saveAndFlush(falsoexp);
        falsoexpSearchRepository.save(falsoexp);

        // Search the falsoexp
        restFalsoexpMockMvc.perform(get("/api/_search/falsoexps?query=id:" + falsoexp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(falsoexp.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCargofal").value(hasItem(DEFAULT_V_CARGOFAL.toString())))
            .andExpect(jsonPath("$.[*].vDemndte").value(hasItem(DEFAULT_V_DEMNDTE.toString())))
            .andExpect(jsonPath("$.[*].vDemandado").value(hasItem(DEFAULT_V_DEMANDADO.toString())))
            .andExpect(jsonPath("$.[*].vDnidmdo").value(hasItem(DEFAULT_V_DNIDMDO.toString())))
            .andExpect(jsonPath("$.[*].vDnidmte").value(hasItem(DEFAULT_V_DNIDMTE.toString())))
            .andExpect(jsonPath("$.[*].vNombres").value(hasItem(DEFAULT_V_NOMBRES.toString())))
            .andExpect(jsonPath("$.[*].vDiffal").value(hasItem(DEFAULT_V_DIFFAL.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Falsoexp.class);
        Falsoexp falsoexp1 = new Falsoexp();
        falsoexp1.setId(1L);
        Falsoexp falsoexp2 = new Falsoexp();
        falsoexp2.setId(falsoexp1.getId());
        assertThat(falsoexp1).isEqualTo(falsoexp2);
        falsoexp2.setId(2L);
        assertThat(falsoexp1).isNotEqualTo(falsoexp2);
        falsoexp1.setId(null);
        assertThat(falsoexp1).isNotEqualTo(falsoexp2);
    }
}
