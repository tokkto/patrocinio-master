package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Denuncia;
import pe.gob.trabajo.repository.DenunciaRepository;
import pe.gob.trabajo.repository.search.DenunciaSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DenunciaResource REST controller.
 *
 * @see DenunciaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class DenunciaResourceIntTest {

    private static final String DEFAULT_V_CODDENU = "AAAAAAAAAA";
    private static final String UPDATED_V_CODDENU = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODSECECO = "A";
    private static final String UPDATED_V_CODSECECO = "B";

    private static final String DEFAULT_V_CODCIU = "AAAAA";
    private static final String UPDATED_V_CODCIU = "BBBBB";

    private static final String DEFAULT_V_DESREMIPE = "AAAAAAAAAA";
    private static final String UPDATED_V_DESREMIPE = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODDEPART = "AAAAAAAAAA";
    private static final String UPDATED_V_CODDEPART = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODPROVIN = "AAAAAAAAAA";
    private static final String UPDATED_V_CODPROVIN = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODDISTRI = "AAAAAAAAAA";
    private static final String UPDATED_V_CODDISTRI = "BBBBBBBBBB";

    private static final String DEFAULT_V_DESVIA = "AAAAAAAAAA";
    private static final String UPDATED_V_DESVIA = "BBBBBBBBBB";

    private static final String DEFAULT_V_DESZONA = "AAAAAAAAAA";
    private static final String UPDATED_V_DESZONA = "BBBBBBBBBB";

    private static final String DEFAULT_V_DIRCOMP = "AAAAAAAAAA";
    private static final String UPDATED_V_DIRCOMP = "BBBBBBBBBB";

    private static final String DEFAULT_V_DIRDENU = "AAAAAAAAAA";
    private static final String UPDATED_V_DIRDENU = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECINITRA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECINITRA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLAGLUN = false;
    private static final Boolean UPDATED_N_FLAGLUN = true;

    private static final Boolean DEFAULT_N_FLAGMAR = false;
    private static final Boolean UPDATED_N_FLAGMAR = true;

    private static final Boolean DEFAULT_N_FLAGMIE = false;
    private static final Boolean UPDATED_N_FLAGMIE = true;

    private static final Boolean DEFAULT_N_FLAGJUE = false;
    private static final Boolean UPDATED_N_FLAGJUE = true;

    private static final Boolean DEFAULT_N_FLAGVIE = false;
    private static final Boolean UPDATED_N_FLAGVIE = true;

    private static final Boolean DEFAULT_N_FLAGSAB = false;
    private static final Boolean UPDATED_N_FLAGSAB = true;

    private static final Boolean DEFAULT_N_FLAGDOM = false;
    private static final Boolean UPDATED_N_FLAGDOM = true;

    private static final String DEFAULT_T_HORAINIT = "AAAAAAAAAA";
    private static final String UPDATED_T_HORAINIT = "BBBBBBBBBB";

    private static final String DEFAULT_T_HORAFINT = "AAAAAAAAAA";
    private static final String UPDATED_T_HORAFINT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_V_FLGTRABA = false;
    private static final Boolean UPDATED_V_FLGTRABA = true;

    private static final Boolean DEFAULT_V_FLGREPRE = false;
    private static final Boolean UPDATED_V_FLGREPRE = true;

    private static final Instant DEFAULT_T_FECCESE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECCESE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_V_DESREPRE = "AAAAAAAAAA";
    private static final String UPDATED_V_DESREPRE = "BBBBBBBBBB";

    private static final String DEFAULT_V_OBSCALIFI = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSCALIFI = "BBBBBBBBBB";

    private static final String DEFAULT_V_OBSFIN = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSFIN = "BBBBBBBBBB";

    private static final String DEFAULT_V_FLGESTADO = "A";
    private static final String UPDATED_V_FLGESTADO = "B";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DenunciaRepository denunciaRepository;

    @Autowired
    private DenunciaSearchRepository denunciaSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDenunciaMockMvc;

    private Denuncia denuncia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DenunciaResource denunciaResource = new DenunciaResource(denunciaRepository, denunciaSearchRepository);
        this.restDenunciaMockMvc = MockMvcBuilders.standaloneSetup(denunciaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Denuncia createEntity(EntityManager em) {
        Denuncia denuncia = new Denuncia()
            .vCoddenu(DEFAULT_V_CODDENU)
            .vCodsececo(DEFAULT_V_CODSECECO)
            .vCodciu(DEFAULT_V_CODCIU)
            .vDesremipe(DEFAULT_V_DESREMIPE)
            .vCoddepart(DEFAULT_V_CODDEPART)
            .vCodprovin(DEFAULT_V_CODPROVIN)
            .vCoddistri(DEFAULT_V_CODDISTRI)
            .vDesvia(DEFAULT_V_DESVIA)
            .vDeszona(DEFAULT_V_DESZONA)
            .vDircomp(DEFAULT_V_DIRCOMP)
            .vDirdenu(DEFAULT_V_DIRDENU)
            .tFecinitra(DEFAULT_T_FECINITRA)
            .nFlaglun(DEFAULT_N_FLAGLUN)
            .nFlagmar(DEFAULT_N_FLAGMAR)
            .nFlagmie(DEFAULT_N_FLAGMIE)
            .nFlagjue(DEFAULT_N_FLAGJUE)
            .nFlagvie(DEFAULT_N_FLAGVIE)
            .nFlagsab(DEFAULT_N_FLAGSAB)
            .nFlagdom(DEFAULT_N_FLAGDOM)
            .tHorainit(DEFAULT_T_HORAINIT)
            .tHorafint(DEFAULT_T_HORAFINT)
            .vFlgtraba(DEFAULT_V_FLGTRABA)
            .vFlgrepre(DEFAULT_V_FLGREPRE)
            .tFeccese(DEFAULT_T_FECCESE)
            .vDesrepre(DEFAULT_V_DESREPRE)
            .vObscalifi(DEFAULT_V_OBSCALIFI)
            .vObsfin(DEFAULT_V_OBSFIN)
            .vFlgestado(DEFAULT_V_FLGESTADO)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return denuncia;
    }

    @Before
    public void initTest() {
        denunciaSearchRepository.deleteAll();
        denuncia = createEntity(em);
    }

    @Test
    @Transactional
    public void createDenuncia() throws Exception {
        int databaseSizeBeforeCreate = denunciaRepository.findAll().size();

        // Create the Denuncia
        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isCreated());

        // Validate the Denuncia in the database
        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeCreate + 1);
        Denuncia testDenuncia = denunciaList.get(denunciaList.size() - 1);
        assertThat(testDenuncia.getvCoddenu()).isEqualTo(DEFAULT_V_CODDENU);
        assertThat(testDenuncia.getvCodsececo()).isEqualTo(DEFAULT_V_CODSECECO);
        assertThat(testDenuncia.getvCodciu()).isEqualTo(DEFAULT_V_CODCIU);
        assertThat(testDenuncia.getvDesremipe()).isEqualTo(DEFAULT_V_DESREMIPE);
        assertThat(testDenuncia.getvCoddepart()).isEqualTo(DEFAULT_V_CODDEPART);
        assertThat(testDenuncia.getvCodprovin()).isEqualTo(DEFAULT_V_CODPROVIN);
        assertThat(testDenuncia.getvCoddistri()).isEqualTo(DEFAULT_V_CODDISTRI);
        assertThat(testDenuncia.getvDesvia()).isEqualTo(DEFAULT_V_DESVIA);
        assertThat(testDenuncia.getvDeszona()).isEqualTo(DEFAULT_V_DESZONA);
        assertThat(testDenuncia.getvDircomp()).isEqualTo(DEFAULT_V_DIRCOMP);
        assertThat(testDenuncia.getvDirdenu()).isEqualTo(DEFAULT_V_DIRDENU);
        assertThat(testDenuncia.gettFecinitra()).isEqualTo(DEFAULT_T_FECINITRA);
        assertThat(testDenuncia.isnFlaglun()).isEqualTo(DEFAULT_N_FLAGLUN);
        assertThat(testDenuncia.isnFlagmar()).isEqualTo(DEFAULT_N_FLAGMAR);
        assertThat(testDenuncia.isnFlagmie()).isEqualTo(DEFAULT_N_FLAGMIE);
        assertThat(testDenuncia.isnFlagjue()).isEqualTo(DEFAULT_N_FLAGJUE);
        assertThat(testDenuncia.isnFlagvie()).isEqualTo(DEFAULT_N_FLAGVIE);
        assertThat(testDenuncia.isnFlagsab()).isEqualTo(DEFAULT_N_FLAGSAB);
        assertThat(testDenuncia.isnFlagdom()).isEqualTo(DEFAULT_N_FLAGDOM);
        assertThat(testDenuncia.gettHorainit()).isEqualTo(DEFAULT_T_HORAINIT);
        assertThat(testDenuncia.gettHorafint()).isEqualTo(DEFAULT_T_HORAFINT);
        assertThat(testDenuncia.isvFlgtraba()).isEqualTo(DEFAULT_V_FLGTRABA);
        assertThat(testDenuncia.isvFlgrepre()).isEqualTo(DEFAULT_V_FLGREPRE);
        assertThat(testDenuncia.gettFeccese()).isEqualTo(DEFAULT_T_FECCESE);
        assertThat(testDenuncia.getvDesrepre()).isEqualTo(DEFAULT_V_DESREPRE);
        assertThat(testDenuncia.getvObscalifi()).isEqualTo(DEFAULT_V_OBSCALIFI);
        assertThat(testDenuncia.getvObsfin()).isEqualTo(DEFAULT_V_OBSFIN);
        assertThat(testDenuncia.getvFlgestado()).isEqualTo(DEFAULT_V_FLGESTADO);
        assertThat(testDenuncia.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testDenuncia.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDenuncia.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDenuncia.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDenuncia.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testDenuncia.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDenuncia.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Denuncia in Elasticsearch
        Denuncia denunciaEs = denunciaSearchRepository.findOne(testDenuncia.getId());
        assertThat(denunciaEs).isEqualToComparingFieldByField(testDenuncia);
    }

    @Test
    @Transactional
    public void createDenunciaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = denunciaRepository.findAll().size();

        // Create the Denuncia with an existing ID
        denuncia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        // Validate the Denuncia in the database
        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCoddenuIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvCoddenu(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodsececoIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvCodsececo(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodciuIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvCodciu(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesremipeIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvDesremipe(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCoddepartIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvCoddepart(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodprovinIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvCodprovin(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCoddistriIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvCoddistri(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesviaIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvDesvia(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDeszonaIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvDeszona(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDircompIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvDircomp(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDirdenuIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvDirdenu(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecinitraIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.settFecinitra(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktHorainitIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.settHorainit(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktHorafintIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.settHorafint(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecceseIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.settFeccese(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDesrepreIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvDesrepre(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvObscalifiIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvObscalifi(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvObsfinIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvObsfin(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgestadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvFlgestado(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setvUsuareg(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.settFecreg(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = denunciaRepository.findAll().size();
        // set the field null
        denuncia.setnSedereg(null);

        // Create the Denuncia, which fails.

        restDenunciaMockMvc.perform(post("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isBadRequest());

        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDenuncias() throws Exception {
        // Initialize the database
        denunciaRepository.saveAndFlush(denuncia);

        // Get all the denunciaList
        restDenunciaMockMvc.perform(get("/api/denuncias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(denuncia.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCoddenu").value(hasItem(DEFAULT_V_CODDENU.toString())))
            .andExpect(jsonPath("$.[*].vCodsececo").value(hasItem(DEFAULT_V_CODSECECO.toString())))
            .andExpect(jsonPath("$.[*].vCodciu").value(hasItem(DEFAULT_V_CODCIU.toString())))
            .andExpect(jsonPath("$.[*].vDesremipe").value(hasItem(DEFAULT_V_DESREMIPE.toString())))
            .andExpect(jsonPath("$.[*].vCoddepart").value(hasItem(DEFAULT_V_CODDEPART.toString())))
            .andExpect(jsonPath("$.[*].vCodprovin").value(hasItem(DEFAULT_V_CODPROVIN.toString())))
            .andExpect(jsonPath("$.[*].vCoddistri").value(hasItem(DEFAULT_V_CODDISTRI.toString())))
            .andExpect(jsonPath("$.[*].vDesvia").value(hasItem(DEFAULT_V_DESVIA.toString())))
            .andExpect(jsonPath("$.[*].vDeszona").value(hasItem(DEFAULT_V_DESZONA.toString())))
            .andExpect(jsonPath("$.[*].vDircomp").value(hasItem(DEFAULT_V_DIRCOMP.toString())))
            .andExpect(jsonPath("$.[*].vDirdenu").value(hasItem(DEFAULT_V_DIRDENU.toString())))
            .andExpect(jsonPath("$.[*].tFecinitra").value(hasItem(DEFAULT_T_FECINITRA.toString())))
            .andExpect(jsonPath("$.[*].nFlaglun").value(hasItem(DEFAULT_N_FLAGLUN.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagmar").value(hasItem(DEFAULT_N_FLAGMAR.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagmie").value(hasItem(DEFAULT_N_FLAGMIE.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagjue").value(hasItem(DEFAULT_N_FLAGJUE.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagvie").value(hasItem(DEFAULT_N_FLAGVIE.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagsab").value(hasItem(DEFAULT_N_FLAGSAB.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagdom").value(hasItem(DEFAULT_N_FLAGDOM.booleanValue())))
            .andExpect(jsonPath("$.[*].tHorainit").value(hasItem(DEFAULT_T_HORAINIT.toString())))
            .andExpect(jsonPath("$.[*].tHorafint").value(hasItem(DEFAULT_T_HORAFINT.toString())))
            .andExpect(jsonPath("$.[*].vFlgtraba").value(hasItem(DEFAULT_V_FLGTRABA.booleanValue())))
            .andExpect(jsonPath("$.[*].vFlgrepre").value(hasItem(DEFAULT_V_FLGREPRE.booleanValue())))
            .andExpect(jsonPath("$.[*].tFeccese").value(hasItem(DEFAULT_T_FECCESE.toString())))
            .andExpect(jsonPath("$.[*].vDesrepre").value(hasItem(DEFAULT_V_DESREPRE.toString())))
            .andExpect(jsonPath("$.[*].vObscalifi").value(hasItem(DEFAULT_V_OBSCALIFI.toString())))
            .andExpect(jsonPath("$.[*].vObsfin").value(hasItem(DEFAULT_V_OBSFIN.toString())))
            .andExpect(jsonPath("$.[*].vFlgestado").value(hasItem(DEFAULT_V_FLGESTADO.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDenuncia() throws Exception {
        // Initialize the database
        denunciaRepository.saveAndFlush(denuncia);

        // Get the denuncia
        restDenunciaMockMvc.perform(get("/api/denuncias/{id}", denuncia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(denuncia.getId().intValue()))
            .andExpect(jsonPath("$.vCoddenu").value(DEFAULT_V_CODDENU.toString()))
            .andExpect(jsonPath("$.vCodsececo").value(DEFAULT_V_CODSECECO.toString()))
            .andExpect(jsonPath("$.vCodciu").value(DEFAULT_V_CODCIU.toString()))
            .andExpect(jsonPath("$.vDesremipe").value(DEFAULT_V_DESREMIPE.toString()))
            .andExpect(jsonPath("$.vCoddepart").value(DEFAULT_V_CODDEPART.toString()))
            .andExpect(jsonPath("$.vCodprovin").value(DEFAULT_V_CODPROVIN.toString()))
            .andExpect(jsonPath("$.vCoddistri").value(DEFAULT_V_CODDISTRI.toString()))
            .andExpect(jsonPath("$.vDesvia").value(DEFAULT_V_DESVIA.toString()))
            .andExpect(jsonPath("$.vDeszona").value(DEFAULT_V_DESZONA.toString()))
            .andExpect(jsonPath("$.vDircomp").value(DEFAULT_V_DIRCOMP.toString()))
            .andExpect(jsonPath("$.vDirdenu").value(DEFAULT_V_DIRDENU.toString()))
            .andExpect(jsonPath("$.tFecinitra").value(DEFAULT_T_FECINITRA.toString()))
            .andExpect(jsonPath("$.nFlaglun").value(DEFAULT_N_FLAGLUN.booleanValue()))
            .andExpect(jsonPath("$.nFlagmar").value(DEFAULT_N_FLAGMAR.booleanValue()))
            .andExpect(jsonPath("$.nFlagmie").value(DEFAULT_N_FLAGMIE.booleanValue()))
            .andExpect(jsonPath("$.nFlagjue").value(DEFAULT_N_FLAGJUE.booleanValue()))
            .andExpect(jsonPath("$.nFlagvie").value(DEFAULT_N_FLAGVIE.booleanValue()))
            .andExpect(jsonPath("$.nFlagsab").value(DEFAULT_N_FLAGSAB.booleanValue()))
            .andExpect(jsonPath("$.nFlagdom").value(DEFAULT_N_FLAGDOM.booleanValue()))
            .andExpect(jsonPath("$.tHorainit").value(DEFAULT_T_HORAINIT.toString()))
            .andExpect(jsonPath("$.tHorafint").value(DEFAULT_T_HORAFINT.toString()))
            .andExpect(jsonPath("$.vFlgtraba").value(DEFAULT_V_FLGTRABA.booleanValue()))
            .andExpect(jsonPath("$.vFlgrepre").value(DEFAULT_V_FLGREPRE.booleanValue()))
            .andExpect(jsonPath("$.tFeccese").value(DEFAULT_T_FECCESE.toString()))
            .andExpect(jsonPath("$.vDesrepre").value(DEFAULT_V_DESREPRE.toString()))
            .andExpect(jsonPath("$.vObscalifi").value(DEFAULT_V_OBSCALIFI.toString()))
            .andExpect(jsonPath("$.vObsfin").value(DEFAULT_V_OBSFIN.toString()))
            .andExpect(jsonPath("$.vFlgestado").value(DEFAULT_V_FLGESTADO.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDenuncia() throws Exception {
        // Get the denuncia
        restDenunciaMockMvc.perform(get("/api/denuncias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDenuncia() throws Exception {
        // Initialize the database
        denunciaRepository.saveAndFlush(denuncia);
        denunciaSearchRepository.save(denuncia);
        int databaseSizeBeforeUpdate = denunciaRepository.findAll().size();

        // Update the denuncia
        Denuncia updatedDenuncia = denunciaRepository.findOne(denuncia.getId());
        updatedDenuncia
            .vCoddenu(UPDATED_V_CODDENU)
            .vCodsececo(UPDATED_V_CODSECECO)
            .vCodciu(UPDATED_V_CODCIU)
            .vDesremipe(UPDATED_V_DESREMIPE)
            .vCoddepart(UPDATED_V_CODDEPART)
            .vCodprovin(UPDATED_V_CODPROVIN)
            .vCoddistri(UPDATED_V_CODDISTRI)
            .vDesvia(UPDATED_V_DESVIA)
            .vDeszona(UPDATED_V_DESZONA)
            .vDircomp(UPDATED_V_DIRCOMP)
            .vDirdenu(UPDATED_V_DIRDENU)
            .tFecinitra(UPDATED_T_FECINITRA)
            .nFlaglun(UPDATED_N_FLAGLUN)
            .nFlagmar(UPDATED_N_FLAGMAR)
            .nFlagmie(UPDATED_N_FLAGMIE)
            .nFlagjue(UPDATED_N_FLAGJUE)
            .nFlagvie(UPDATED_N_FLAGVIE)
            .nFlagsab(UPDATED_N_FLAGSAB)
            .nFlagdom(UPDATED_N_FLAGDOM)
            .tHorainit(UPDATED_T_HORAINIT)
            .tHorafint(UPDATED_T_HORAFINT)
            .vFlgtraba(UPDATED_V_FLGTRABA)
            .vFlgrepre(UPDATED_V_FLGREPRE)
            .tFeccese(UPDATED_T_FECCESE)
            .vDesrepre(UPDATED_V_DESREPRE)
            .vObscalifi(UPDATED_V_OBSCALIFI)
            .vObsfin(UPDATED_V_OBSFIN)
            .vFlgestado(UPDATED_V_FLGESTADO)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDenunciaMockMvc.perform(put("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDenuncia)))
            .andExpect(status().isOk());

        // Validate the Denuncia in the database
        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeUpdate);
        Denuncia testDenuncia = denunciaList.get(denunciaList.size() - 1);
        assertThat(testDenuncia.getvCoddenu()).isEqualTo(UPDATED_V_CODDENU);
        assertThat(testDenuncia.getvCodsececo()).isEqualTo(UPDATED_V_CODSECECO);
        assertThat(testDenuncia.getvCodciu()).isEqualTo(UPDATED_V_CODCIU);
        assertThat(testDenuncia.getvDesremipe()).isEqualTo(UPDATED_V_DESREMIPE);
        assertThat(testDenuncia.getvCoddepart()).isEqualTo(UPDATED_V_CODDEPART);
        assertThat(testDenuncia.getvCodprovin()).isEqualTo(UPDATED_V_CODPROVIN);
        assertThat(testDenuncia.getvCoddistri()).isEqualTo(UPDATED_V_CODDISTRI);
        assertThat(testDenuncia.getvDesvia()).isEqualTo(UPDATED_V_DESVIA);
        assertThat(testDenuncia.getvDeszona()).isEqualTo(UPDATED_V_DESZONA);
        assertThat(testDenuncia.getvDircomp()).isEqualTo(UPDATED_V_DIRCOMP);
        assertThat(testDenuncia.getvDirdenu()).isEqualTo(UPDATED_V_DIRDENU);
        assertThat(testDenuncia.gettFecinitra()).isEqualTo(UPDATED_T_FECINITRA);
        assertThat(testDenuncia.isnFlaglun()).isEqualTo(UPDATED_N_FLAGLUN);
        assertThat(testDenuncia.isnFlagmar()).isEqualTo(UPDATED_N_FLAGMAR);
        assertThat(testDenuncia.isnFlagmie()).isEqualTo(UPDATED_N_FLAGMIE);
        assertThat(testDenuncia.isnFlagjue()).isEqualTo(UPDATED_N_FLAGJUE);
        assertThat(testDenuncia.isnFlagvie()).isEqualTo(UPDATED_N_FLAGVIE);
        assertThat(testDenuncia.isnFlagsab()).isEqualTo(UPDATED_N_FLAGSAB);
        assertThat(testDenuncia.isnFlagdom()).isEqualTo(UPDATED_N_FLAGDOM);
        assertThat(testDenuncia.gettHorainit()).isEqualTo(UPDATED_T_HORAINIT);
        assertThat(testDenuncia.gettHorafint()).isEqualTo(UPDATED_T_HORAFINT);
        assertThat(testDenuncia.isvFlgtraba()).isEqualTo(UPDATED_V_FLGTRABA);
        assertThat(testDenuncia.isvFlgrepre()).isEqualTo(UPDATED_V_FLGREPRE);
        assertThat(testDenuncia.gettFeccese()).isEqualTo(UPDATED_T_FECCESE);
        assertThat(testDenuncia.getvDesrepre()).isEqualTo(UPDATED_V_DESREPRE);
        assertThat(testDenuncia.getvObscalifi()).isEqualTo(UPDATED_V_OBSCALIFI);
        assertThat(testDenuncia.getvObsfin()).isEqualTo(UPDATED_V_OBSFIN);
        assertThat(testDenuncia.getvFlgestado()).isEqualTo(UPDATED_V_FLGESTADO);
        assertThat(testDenuncia.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testDenuncia.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDenuncia.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDenuncia.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDenuncia.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testDenuncia.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDenuncia.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Denuncia in Elasticsearch
        Denuncia denunciaEs = denunciaSearchRepository.findOne(testDenuncia.getId());
        assertThat(denunciaEs).isEqualToComparingFieldByField(testDenuncia);
    }

    @Test
    @Transactional
    public void updateNonExistingDenuncia() throws Exception {
        int databaseSizeBeforeUpdate = denunciaRepository.findAll().size();

        // Create the Denuncia

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDenunciaMockMvc.perform(put("/api/denuncias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(denuncia)))
            .andExpect(status().isCreated());

        // Validate the Denuncia in the database
        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDenuncia() throws Exception {
        // Initialize the database
        denunciaRepository.saveAndFlush(denuncia);
        denunciaSearchRepository.save(denuncia);
        int databaseSizeBeforeDelete = denunciaRepository.findAll().size();

        // Get the denuncia
        restDenunciaMockMvc.perform(delete("/api/denuncias/{id}", denuncia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean denunciaExistsInEs = denunciaSearchRepository.exists(denuncia.getId());
        assertThat(denunciaExistsInEs).isFalse();

        // Validate the database is empty
        List<Denuncia> denunciaList = denunciaRepository.findAll();
        assertThat(denunciaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDenuncia() throws Exception {
        // Initialize the database
        denunciaRepository.saveAndFlush(denuncia);
        denunciaSearchRepository.save(denuncia);

        // Search the denuncia
        restDenunciaMockMvc.perform(get("/api/_search/denuncias?query=id:" + denuncia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(denuncia.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCoddenu").value(hasItem(DEFAULT_V_CODDENU.toString())))
            .andExpect(jsonPath("$.[*].vCodsececo").value(hasItem(DEFAULT_V_CODSECECO.toString())))
            .andExpect(jsonPath("$.[*].vCodciu").value(hasItem(DEFAULT_V_CODCIU.toString())))
            .andExpect(jsonPath("$.[*].vDesremipe").value(hasItem(DEFAULT_V_DESREMIPE.toString())))
            .andExpect(jsonPath("$.[*].vCoddepart").value(hasItem(DEFAULT_V_CODDEPART.toString())))
            .andExpect(jsonPath("$.[*].vCodprovin").value(hasItem(DEFAULT_V_CODPROVIN.toString())))
            .andExpect(jsonPath("$.[*].vCoddistri").value(hasItem(DEFAULT_V_CODDISTRI.toString())))
            .andExpect(jsonPath("$.[*].vDesvia").value(hasItem(DEFAULT_V_DESVIA.toString())))
            .andExpect(jsonPath("$.[*].vDeszona").value(hasItem(DEFAULT_V_DESZONA.toString())))
            .andExpect(jsonPath("$.[*].vDircomp").value(hasItem(DEFAULT_V_DIRCOMP.toString())))
            .andExpect(jsonPath("$.[*].vDirdenu").value(hasItem(DEFAULT_V_DIRDENU.toString())))
            .andExpect(jsonPath("$.[*].tFecinitra").value(hasItem(DEFAULT_T_FECINITRA.toString())))
            .andExpect(jsonPath("$.[*].nFlaglun").value(hasItem(DEFAULT_N_FLAGLUN.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagmar").value(hasItem(DEFAULT_N_FLAGMAR.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagmie").value(hasItem(DEFAULT_N_FLAGMIE.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagjue").value(hasItem(DEFAULT_N_FLAGJUE.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagvie").value(hasItem(DEFAULT_N_FLAGVIE.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagsab").value(hasItem(DEFAULT_N_FLAGSAB.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlagdom").value(hasItem(DEFAULT_N_FLAGDOM.booleanValue())))
            .andExpect(jsonPath("$.[*].tHorainit").value(hasItem(DEFAULT_T_HORAINIT.toString())))
            .andExpect(jsonPath("$.[*].tHorafint").value(hasItem(DEFAULT_T_HORAFINT.toString())))
            .andExpect(jsonPath("$.[*].vFlgtraba").value(hasItem(DEFAULT_V_FLGTRABA.booleanValue())))
            .andExpect(jsonPath("$.[*].vFlgrepre").value(hasItem(DEFAULT_V_FLGREPRE.booleanValue())))
            .andExpect(jsonPath("$.[*].tFeccese").value(hasItem(DEFAULT_T_FECCESE.toString())))
            .andExpect(jsonPath("$.[*].vDesrepre").value(hasItem(DEFAULT_V_DESREPRE.toString())))
            .andExpect(jsonPath("$.[*].vObscalifi").value(hasItem(DEFAULT_V_OBSCALIFI.toString())))
            .andExpect(jsonPath("$.[*].vObsfin").value(hasItem(DEFAULT_V_OBSFIN.toString())))
            .andExpect(jsonPath("$.[*].vFlgestado").value(hasItem(DEFAULT_V_FLGESTADO.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Denuncia.class);
        Denuncia denuncia1 = new Denuncia();
        denuncia1.setId(1L);
        Denuncia denuncia2 = new Denuncia();
        denuncia2.setId(denuncia1.getId());
        assertThat(denuncia1).isEqualTo(denuncia2);
        denuncia2.setId(2L);
        assertThat(denuncia1).isNotEqualTo(denuncia2);
        denuncia1.setId(null);
        assertThat(denuncia1).isNotEqualTo(denuncia2);
    }
}
