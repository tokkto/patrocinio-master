package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Calidenu;
import pe.gob.trabajo.repository.CalidenuRepository;
import pe.gob.trabajo.repository.search.CalidenuSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CalidenuResource REST controller.
 *
 * @see CalidenuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class CalidenuResourceIntTest {

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private CalidenuRepository calidenuRepository;

    @Autowired
    private CalidenuSearchRepository calidenuSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCalidenuMockMvc;

    private Calidenu calidenu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CalidenuResource calidenuResource = new CalidenuResource(calidenuRepository, calidenuSearchRepository);
        this.restCalidenuMockMvc = MockMvcBuilders.standaloneSetup(calidenuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Calidenu createEntity(EntityManager em) {
        Calidenu calidenu = new Calidenu()
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return calidenu;
    }

    @Before
    public void initTest() {
        calidenuSearchRepository.deleteAll();
        calidenu = createEntity(em);
    }

    @Test
    @Transactional
    public void createCalidenu() throws Exception {
        int databaseSizeBeforeCreate = calidenuRepository.findAll().size();

        // Create the Calidenu
        restCalidenuMockMvc.perform(post("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calidenu)))
            .andExpect(status().isCreated());

        // Validate the Calidenu in the database
        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeCreate + 1);
        Calidenu testCalidenu = calidenuList.get(calidenuList.size() - 1);
        assertThat(testCalidenu.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testCalidenu.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testCalidenu.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testCalidenu.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testCalidenu.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testCalidenu.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testCalidenu.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Calidenu in Elasticsearch
        Calidenu calidenuEs = calidenuSearchRepository.findOne(testCalidenu.getId());
        assertThat(calidenuEs).isEqualToComparingFieldByField(testCalidenu);
    }

    @Test
    @Transactional
    public void createCalidenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = calidenuRepository.findAll().size();

        // Create the Calidenu with an existing ID
        calidenu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCalidenuMockMvc.perform(post("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calidenu)))
            .andExpect(status().isBadRequest());

        // Validate the Calidenu in the database
        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = calidenuRepository.findAll().size();
        // set the field null
        calidenu.setvUsuareg(null);

        // Create the Calidenu, which fails.

        restCalidenuMockMvc.perform(post("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calidenu)))
            .andExpect(status().isBadRequest());

        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = calidenuRepository.findAll().size();
        // set the field null
        calidenu.settFecreg(null);

        // Create the Calidenu, which fails.

        restCalidenuMockMvc.perform(post("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calidenu)))
            .andExpect(status().isBadRequest());

        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = calidenuRepository.findAll().size();
        // set the field null
        calidenu.setnSedereg(null);

        // Create the Calidenu, which fails.

        restCalidenuMockMvc.perform(post("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calidenu)))
            .andExpect(status().isBadRequest());

        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCalidenus() throws Exception {
        // Initialize the database
        calidenuRepository.saveAndFlush(calidenu);

        // Get all the calidenuList
        restCalidenuMockMvc.perform(get("/api/calidenus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(calidenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getCalidenu() throws Exception {
        // Initialize the database
        calidenuRepository.saveAndFlush(calidenu);

        // Get the calidenu
        restCalidenuMockMvc.perform(get("/api/calidenus/{id}", calidenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(calidenu.getId().intValue()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingCalidenu() throws Exception {
        // Get the calidenu
        restCalidenuMockMvc.perform(get("/api/calidenus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCalidenu() throws Exception {
        // Initialize the database
        calidenuRepository.saveAndFlush(calidenu);
        calidenuSearchRepository.save(calidenu);
        int databaseSizeBeforeUpdate = calidenuRepository.findAll().size();

        // Update the calidenu
        Calidenu updatedCalidenu = calidenuRepository.findOne(calidenu.getId());
        updatedCalidenu
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restCalidenuMockMvc.perform(put("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCalidenu)))
            .andExpect(status().isOk());

        // Validate the Calidenu in the database
        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeUpdate);
        Calidenu testCalidenu = calidenuList.get(calidenuList.size() - 1);
        assertThat(testCalidenu.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testCalidenu.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testCalidenu.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testCalidenu.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testCalidenu.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testCalidenu.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testCalidenu.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Calidenu in Elasticsearch
        Calidenu calidenuEs = calidenuSearchRepository.findOne(testCalidenu.getId());
        assertThat(calidenuEs).isEqualToComparingFieldByField(testCalidenu);
    }

    @Test
    @Transactional
    public void updateNonExistingCalidenu() throws Exception {
        int databaseSizeBeforeUpdate = calidenuRepository.findAll().size();

        // Create the Calidenu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCalidenuMockMvc.perform(put("/api/calidenus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calidenu)))
            .andExpect(status().isCreated());

        // Validate the Calidenu in the database
        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCalidenu() throws Exception {
        // Initialize the database
        calidenuRepository.saveAndFlush(calidenu);
        calidenuSearchRepository.save(calidenu);
        int databaseSizeBeforeDelete = calidenuRepository.findAll().size();

        // Get the calidenu
        restCalidenuMockMvc.perform(delete("/api/calidenus/{id}", calidenu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean calidenuExistsInEs = calidenuSearchRepository.exists(calidenu.getId());
        assertThat(calidenuExistsInEs).isFalse();

        // Validate the database is empty
        List<Calidenu> calidenuList = calidenuRepository.findAll();
        assertThat(calidenuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCalidenu() throws Exception {
        // Initialize the database
        calidenuRepository.saveAndFlush(calidenu);
        calidenuSearchRepository.save(calidenu);

        // Search the calidenu
        restCalidenuMockMvc.perform(get("/api/_search/calidenus?query=id:" + calidenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(calidenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Calidenu.class);
        Calidenu calidenu1 = new Calidenu();
        calidenu1.setId(1L);
        Calidenu calidenu2 = new Calidenu();
        calidenu2.setId(calidenu1.getId());
        assertThat(calidenu1).isEqualTo(calidenu2);
        calidenu2.setId(2L);
        assertThat(calidenu1).isNotEqualTo(calidenu2);
        calidenu1.setId(null);
        assertThat(calidenu1).isNotEqualTo(calidenu2);
    }
}
