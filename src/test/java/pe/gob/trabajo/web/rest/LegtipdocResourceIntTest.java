package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Legtipdoc;
import pe.gob.trabajo.repository.LegtipdocRepository;
import pe.gob.trabajo.repository.search.LegtipdocSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LegtipdocResource REST controller.
 *
 * @see LegtipdocResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class LegtipdocResourceIntTest {

    private static final String DEFAULT_V_CODREG = "AA";
    private static final String UPDATED_V_CODREG = "BB";

    private static final String DEFAULT_V_CODZON = "AA";
    private static final String UPDATED_V_CODZON = "BB";

    private static final Integer DEFAULT_N_CORREL = 1;
    private static final Integer UPDATED_N_CORREL = 2;

    private static final String DEFAULT_V_CODSIS = "AAA";
    private static final String UPDATED_V_CODSIS = "BBB";

    private static final String DEFAULT_V_ASUNDOC = "AAAAAAAAAA";
    private static final String UPDATED_V_ASUNDOC = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECDOC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECDOC = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_N_MONPAG = 1;
    private static final Integer UPDATED_N_MONPAG = 2;

    private static final LocalDate DEFAULT_D_FECENTR = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECENTR = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_D_FECDEV = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECDEV = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_D_FECRECJUZ = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECRECJUZ = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_CODUSUREG = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUREG = "BBBBBBBBBB";

    private static final String DEFAULT_V_HOSTREG = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTREG = "BBBBBBBBBB";

    private static final String DEFAULT_V_CODUSUMOD = "AAAAAAAAAA";
    private static final String UPDATED_V_CODUSUMOD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECMOD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECMOD = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_HOSTMOD = "AAAAAAAAAA";
    private static final String UPDATED_V_HOSTMOD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECCIT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECCIT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_FLGFUND = "A";
    private static final String UPDATED_V_FLGFUND = "B";

    private static final String DEFAULT_V_FLGASIS = "A";
    private static final String UPDATED_V_FLGASIS = "B";

    private static final Integer DEFAULT_N_NUMRES = 1;
    private static final Integer UPDATED_N_NUMRES = 2;

    private static final Integer DEFAULT_N_MONDOL = 1;
    private static final Integer UPDATED_N_MONDOL = 2;

    private static final String DEFAULT_V_CODLOC = "AAAAAA";
    private static final String UPDATED_V_CODLOC = "BBBBBB";

    private static final String DEFAULT_V_FLGCONC = "A";
    private static final String UPDATED_V_FLGCONC = "B";

    private static final String DEFAULT_V_DETTDOC = "AA";
    private static final String UPDATED_V_DETTDOC = "BB";

    private static final LocalDate DEFAULT_D_FECDOCREQ = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECDOCREQ = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private LegtipdocRepository legtipdocRepository;

    @Autowired
    private LegtipdocSearchRepository legtipdocSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLegtipdocMockMvc;

    private Legtipdoc legtipdoc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LegtipdocResource legtipdocResource = new LegtipdocResource(legtipdocRepository, legtipdocSearchRepository);
        this.restLegtipdocMockMvc = MockMvcBuilders.standaloneSetup(legtipdocResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Legtipdoc createEntity(EntityManager em) {
        Legtipdoc legtipdoc = new Legtipdoc()
            .vCodreg(DEFAULT_V_CODREG)
            .vCodzon(DEFAULT_V_CODZON)
            .nCorrel(DEFAULT_N_CORREL)
            .vCodsis(DEFAULT_V_CODSIS)
            .vAsundoc(DEFAULT_V_ASUNDOC)
            .dFecdoc(DEFAULT_D_FECDOC)
            .nMonpag(DEFAULT_N_MONPAG)
            .dFecentr(DEFAULT_D_FECENTR)
            .dFecdev(DEFAULT_D_FECDEV)
            .dFecrecjuz(DEFAULT_D_FECRECJUZ)
            .vCodusureg(DEFAULT_V_CODUSUREG)
            .vHostreg(DEFAULT_V_HOSTREG)
            .vCodusumod(DEFAULT_V_CODUSUMOD)
            .dFecmod(DEFAULT_D_FECMOD)
            .vHostmod(DEFAULT_V_HOSTMOD)
            .dFeccit(DEFAULT_D_FECCIT)
            .vFlgfund(DEFAULT_V_FLGFUND)
            .vFlgasis(DEFAULT_V_FLGASIS)
            .nNumres(DEFAULT_N_NUMRES)
            .nMondol(DEFAULT_N_MONDOL)
            .vCodloc(DEFAULT_V_CODLOC)
            .vFlgconc(DEFAULT_V_FLGCONC)
            .vDettdoc(DEFAULT_V_DETTDOC)
            .dFecdocreq(DEFAULT_D_FECDOCREQ)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return legtipdoc;
    }

    @Before
    public void initTest() {
        legtipdocSearchRepository.deleteAll();
        legtipdoc = createEntity(em);
    }

    @Test
    @Transactional
    public void createLegtipdoc() throws Exception {
        int databaseSizeBeforeCreate = legtipdocRepository.findAll().size();

        // Create the Legtipdoc
        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isCreated());

        // Validate the Legtipdoc in the database
        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeCreate + 1);
        Legtipdoc testLegtipdoc = legtipdocList.get(legtipdocList.size() - 1);
        assertThat(testLegtipdoc.getvCodreg()).isEqualTo(DEFAULT_V_CODREG);
        assertThat(testLegtipdoc.getvCodzon()).isEqualTo(DEFAULT_V_CODZON);
        assertThat(testLegtipdoc.getnCorrel()).isEqualTo(DEFAULT_N_CORREL);
        assertThat(testLegtipdoc.getvCodsis()).isEqualTo(DEFAULT_V_CODSIS);
        assertThat(testLegtipdoc.getvAsundoc()).isEqualTo(DEFAULT_V_ASUNDOC);
        assertThat(testLegtipdoc.getdFecdoc()).isEqualTo(DEFAULT_D_FECDOC);
        assertThat(testLegtipdoc.getnMonpag()).isEqualTo(DEFAULT_N_MONPAG);
        assertThat(testLegtipdoc.getdFecentr()).isEqualTo(DEFAULT_D_FECENTR);
        assertThat(testLegtipdoc.getdFecdev()).isEqualTo(DEFAULT_D_FECDEV);
        assertThat(testLegtipdoc.getdFecrecjuz()).isEqualTo(DEFAULT_D_FECRECJUZ);
        assertThat(testLegtipdoc.getvCodusureg()).isEqualTo(DEFAULT_V_CODUSUREG);
        assertThat(testLegtipdoc.getvHostreg()).isEqualTo(DEFAULT_V_HOSTREG);
        assertThat(testLegtipdoc.getvCodusumod()).isEqualTo(DEFAULT_V_CODUSUMOD);
        assertThat(testLegtipdoc.getdFecmod()).isEqualTo(DEFAULT_D_FECMOD);
        assertThat(testLegtipdoc.getvHostmod()).isEqualTo(DEFAULT_V_HOSTMOD);
        assertThat(testLegtipdoc.getdFeccit()).isEqualTo(DEFAULT_D_FECCIT);
        assertThat(testLegtipdoc.getvFlgfund()).isEqualTo(DEFAULT_V_FLGFUND);
        assertThat(testLegtipdoc.getvFlgasis()).isEqualTo(DEFAULT_V_FLGASIS);
        assertThat(testLegtipdoc.getnNumres()).isEqualTo(DEFAULT_N_NUMRES);
        assertThat(testLegtipdoc.getnMondol()).isEqualTo(DEFAULT_N_MONDOL);
        assertThat(testLegtipdoc.getvCodloc()).isEqualTo(DEFAULT_V_CODLOC);
        assertThat(testLegtipdoc.getvFlgconc()).isEqualTo(DEFAULT_V_FLGCONC);
        assertThat(testLegtipdoc.getvDettdoc()).isEqualTo(DEFAULT_V_DETTDOC);
        assertThat(testLegtipdoc.getdFecdocreq()).isEqualTo(DEFAULT_D_FECDOCREQ);
        assertThat(testLegtipdoc.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testLegtipdoc.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testLegtipdoc.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testLegtipdoc.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testLegtipdoc.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testLegtipdoc.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testLegtipdoc.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Legtipdoc in Elasticsearch
        Legtipdoc legtipdocEs = legtipdocSearchRepository.findOne(testLegtipdoc.getId());
        assertThat(legtipdocEs).isEqualToComparingFieldByField(testLegtipdoc);
    }

    @Test
    @Transactional
    public void createLegtipdocWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = legtipdocRepository.findAll().size();

        // Create the Legtipdoc with an existing ID
        legtipdoc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        // Validate the Legtipdoc in the database
        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvCodregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvCodreg(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodzonIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvCodzon(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknCorrelIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnCorrel(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodsisIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvCodsis(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvAsundocIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvAsundoc(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknMonpagIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnMonpag(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusuregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvCodusureg(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvHostreg(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodusumodIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvCodusumod(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvHostmodIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvHostmod(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgfundIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvFlgfund(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgasisIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvFlgasis(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknNumresIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnNumres(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknMondolIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnMondol(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvCodlocIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvCodloc(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvFlgconcIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvFlgconc(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvDettdocIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setvDettdoc(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnUsuareg(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.settFecreg(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnFlgactivo(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = legtipdocRepository.findAll().size();
        // set the field null
        legtipdoc.setnSedereg(null);

        // Create the Legtipdoc, which fails.

        restLegtipdocMockMvc.perform(post("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isBadRequest());

        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLegtipdocs() throws Exception {
        // Initialize the database
        legtipdocRepository.saveAndFlush(legtipdoc);

        // Get all the legtipdocList
        restLegtipdocMockMvc.perform(get("/api/legtipdocs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(legtipdoc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].vAsundoc").value(hasItem(DEFAULT_V_ASUNDOC.toString())))
            .andExpect(jsonPath("$.[*].dFecdoc").value(hasItem(DEFAULT_D_FECDOC.toString())))
            .andExpect(jsonPath("$.[*].nMonpag").value(hasItem(DEFAULT_N_MONPAG)))
            .andExpect(jsonPath("$.[*].dFecentr").value(hasItem(DEFAULT_D_FECENTR.toString())))
            .andExpect(jsonPath("$.[*].dFecdev").value(hasItem(DEFAULT_D_FECDEV.toString())))
            .andExpect(jsonPath("$.[*].dFecrecjuz").value(hasItem(DEFAULT_D_FECRECJUZ.toString())))
            .andExpect(jsonPath("$.[*].vCodusureg").value(hasItem(DEFAULT_V_CODUSUREG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].vCodusumod").value(hasItem(DEFAULT_V_CODUSUMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecmod").value(hasItem(DEFAULT_D_FECMOD.toString())))
            .andExpect(jsonPath("$.[*].vHostmod").value(hasItem(DEFAULT_V_HOSTMOD.toString())))
            .andExpect(jsonPath("$.[*].dFeccit").value(hasItem(DEFAULT_D_FECCIT.toString())))
            .andExpect(jsonPath("$.[*].vFlgfund").value(hasItem(DEFAULT_V_FLGFUND.toString())))
            .andExpect(jsonPath("$.[*].vFlgasis").value(hasItem(DEFAULT_V_FLGASIS.toString())))
            .andExpect(jsonPath("$.[*].nNumres").value(hasItem(DEFAULT_N_NUMRES)))
            .andExpect(jsonPath("$.[*].nMondol").value(hasItem(DEFAULT_N_MONDOL)))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].vFlgconc").value(hasItem(DEFAULT_V_FLGCONC.toString())))
            .andExpect(jsonPath("$.[*].vDettdoc").value(hasItem(DEFAULT_V_DETTDOC.toString())))
            .andExpect(jsonPath("$.[*].dFecdocreq").value(hasItem(DEFAULT_D_FECDOCREQ.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getLegtipdoc() throws Exception {
        // Initialize the database
        legtipdocRepository.saveAndFlush(legtipdoc);

        // Get the legtipdoc
        restLegtipdocMockMvc.perform(get("/api/legtipdocs/{id}", legtipdoc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(legtipdoc.getId().intValue()))
            .andExpect(jsonPath("$.vCodreg").value(DEFAULT_V_CODREG.toString()))
            .andExpect(jsonPath("$.vCodzon").value(DEFAULT_V_CODZON.toString()))
            .andExpect(jsonPath("$.nCorrel").value(DEFAULT_N_CORREL))
            .andExpect(jsonPath("$.vCodsis").value(DEFAULT_V_CODSIS.toString()))
            .andExpect(jsonPath("$.vAsundoc").value(DEFAULT_V_ASUNDOC.toString()))
            .andExpect(jsonPath("$.dFecdoc").value(DEFAULT_D_FECDOC.toString()))
            .andExpect(jsonPath("$.nMonpag").value(DEFAULT_N_MONPAG))
            .andExpect(jsonPath("$.dFecentr").value(DEFAULT_D_FECENTR.toString()))
            .andExpect(jsonPath("$.dFecdev").value(DEFAULT_D_FECDEV.toString()))
            .andExpect(jsonPath("$.dFecrecjuz").value(DEFAULT_D_FECRECJUZ.toString()))
            .andExpect(jsonPath("$.vCodusureg").value(DEFAULT_V_CODUSUREG.toString()))
            .andExpect(jsonPath("$.vHostreg").value(DEFAULT_V_HOSTREG.toString()))
            .andExpect(jsonPath("$.vCodusumod").value(DEFAULT_V_CODUSUMOD.toString()))
            .andExpect(jsonPath("$.dFecmod").value(DEFAULT_D_FECMOD.toString()))
            .andExpect(jsonPath("$.vHostmod").value(DEFAULT_V_HOSTMOD.toString()))
            .andExpect(jsonPath("$.dFeccit").value(DEFAULT_D_FECCIT.toString()))
            .andExpect(jsonPath("$.vFlgfund").value(DEFAULT_V_FLGFUND.toString()))
            .andExpect(jsonPath("$.vFlgasis").value(DEFAULT_V_FLGASIS.toString()))
            .andExpect(jsonPath("$.nNumres").value(DEFAULT_N_NUMRES))
            .andExpect(jsonPath("$.nMondol").value(DEFAULT_N_MONDOL))
            .andExpect(jsonPath("$.vCodloc").value(DEFAULT_V_CODLOC.toString()))
            .andExpect(jsonPath("$.vFlgconc").value(DEFAULT_V_FLGCONC.toString()))
            .andExpect(jsonPath("$.vDettdoc").value(DEFAULT_V_DETTDOC.toString()))
            .andExpect(jsonPath("$.dFecdocreq").value(DEFAULT_D_FECDOCREQ.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingLegtipdoc() throws Exception {
        // Get the legtipdoc
        restLegtipdocMockMvc.perform(get("/api/legtipdocs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLegtipdoc() throws Exception {
        // Initialize the database
        legtipdocRepository.saveAndFlush(legtipdoc);
        legtipdocSearchRepository.save(legtipdoc);
        int databaseSizeBeforeUpdate = legtipdocRepository.findAll().size();

        // Update the legtipdoc
        Legtipdoc updatedLegtipdoc = legtipdocRepository.findOne(legtipdoc.getId());
        updatedLegtipdoc
            .vCodreg(UPDATED_V_CODREG)
            .vCodzon(UPDATED_V_CODZON)
            .nCorrel(UPDATED_N_CORREL)
            .vCodsis(UPDATED_V_CODSIS)
            .vAsundoc(UPDATED_V_ASUNDOC)
            .dFecdoc(UPDATED_D_FECDOC)
            .nMonpag(UPDATED_N_MONPAG)
            .dFecentr(UPDATED_D_FECENTR)
            .dFecdev(UPDATED_D_FECDEV)
            .dFecrecjuz(UPDATED_D_FECRECJUZ)
            .vCodusureg(UPDATED_V_CODUSUREG)
            .vHostreg(UPDATED_V_HOSTREG)
            .vCodusumod(UPDATED_V_CODUSUMOD)
            .dFecmod(UPDATED_D_FECMOD)
            .vHostmod(UPDATED_V_HOSTMOD)
            .dFeccit(UPDATED_D_FECCIT)
            .vFlgfund(UPDATED_V_FLGFUND)
            .vFlgasis(UPDATED_V_FLGASIS)
            .nNumres(UPDATED_N_NUMRES)
            .nMondol(UPDATED_N_MONDOL)
            .vCodloc(UPDATED_V_CODLOC)
            .vFlgconc(UPDATED_V_FLGCONC)
            .vDettdoc(UPDATED_V_DETTDOC)
            .dFecdocreq(UPDATED_D_FECDOCREQ)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restLegtipdocMockMvc.perform(put("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLegtipdoc)))
            .andExpect(status().isOk());

        // Validate the Legtipdoc in the database
        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeUpdate);
        Legtipdoc testLegtipdoc = legtipdocList.get(legtipdocList.size() - 1);
        assertThat(testLegtipdoc.getvCodreg()).isEqualTo(UPDATED_V_CODREG);
        assertThat(testLegtipdoc.getvCodzon()).isEqualTo(UPDATED_V_CODZON);
        assertThat(testLegtipdoc.getnCorrel()).isEqualTo(UPDATED_N_CORREL);
        assertThat(testLegtipdoc.getvCodsis()).isEqualTo(UPDATED_V_CODSIS);
        assertThat(testLegtipdoc.getvAsundoc()).isEqualTo(UPDATED_V_ASUNDOC);
        assertThat(testLegtipdoc.getdFecdoc()).isEqualTo(UPDATED_D_FECDOC);
        assertThat(testLegtipdoc.getnMonpag()).isEqualTo(UPDATED_N_MONPAG);
        assertThat(testLegtipdoc.getdFecentr()).isEqualTo(UPDATED_D_FECENTR);
        assertThat(testLegtipdoc.getdFecdev()).isEqualTo(UPDATED_D_FECDEV);
        assertThat(testLegtipdoc.getdFecrecjuz()).isEqualTo(UPDATED_D_FECRECJUZ);
        assertThat(testLegtipdoc.getvCodusureg()).isEqualTo(UPDATED_V_CODUSUREG);
        assertThat(testLegtipdoc.getvHostreg()).isEqualTo(UPDATED_V_HOSTREG);
        assertThat(testLegtipdoc.getvCodusumod()).isEqualTo(UPDATED_V_CODUSUMOD);
        assertThat(testLegtipdoc.getdFecmod()).isEqualTo(UPDATED_D_FECMOD);
        assertThat(testLegtipdoc.getvHostmod()).isEqualTo(UPDATED_V_HOSTMOD);
        assertThat(testLegtipdoc.getdFeccit()).isEqualTo(UPDATED_D_FECCIT);
        assertThat(testLegtipdoc.getvFlgfund()).isEqualTo(UPDATED_V_FLGFUND);
        assertThat(testLegtipdoc.getvFlgasis()).isEqualTo(UPDATED_V_FLGASIS);
        assertThat(testLegtipdoc.getnNumres()).isEqualTo(UPDATED_N_NUMRES);
        assertThat(testLegtipdoc.getnMondol()).isEqualTo(UPDATED_N_MONDOL);
        assertThat(testLegtipdoc.getvCodloc()).isEqualTo(UPDATED_V_CODLOC);
        assertThat(testLegtipdoc.getvFlgconc()).isEqualTo(UPDATED_V_FLGCONC);
        assertThat(testLegtipdoc.getvDettdoc()).isEqualTo(UPDATED_V_DETTDOC);
        assertThat(testLegtipdoc.getdFecdocreq()).isEqualTo(UPDATED_D_FECDOCREQ);
        assertThat(testLegtipdoc.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testLegtipdoc.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testLegtipdoc.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testLegtipdoc.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testLegtipdoc.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testLegtipdoc.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testLegtipdoc.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Legtipdoc in Elasticsearch
        Legtipdoc legtipdocEs = legtipdocSearchRepository.findOne(testLegtipdoc.getId());
        assertThat(legtipdocEs).isEqualToComparingFieldByField(testLegtipdoc);
    }

    @Test
    @Transactional
    public void updateNonExistingLegtipdoc() throws Exception {
        int databaseSizeBeforeUpdate = legtipdocRepository.findAll().size();

        // Create the Legtipdoc

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLegtipdocMockMvc.perform(put("/api/legtipdocs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(legtipdoc)))
            .andExpect(status().isCreated());

        // Validate the Legtipdoc in the database
        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLegtipdoc() throws Exception {
        // Initialize the database
        legtipdocRepository.saveAndFlush(legtipdoc);
        legtipdocSearchRepository.save(legtipdoc);
        int databaseSizeBeforeDelete = legtipdocRepository.findAll().size();

        // Get the legtipdoc
        restLegtipdocMockMvc.perform(delete("/api/legtipdocs/{id}", legtipdoc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean legtipdocExistsInEs = legtipdocSearchRepository.exists(legtipdoc.getId());
        assertThat(legtipdocExistsInEs).isFalse();

        // Validate the database is empty
        List<Legtipdoc> legtipdocList = legtipdocRepository.findAll();
        assertThat(legtipdocList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLegtipdoc() throws Exception {
        // Initialize the database
        legtipdocRepository.saveAndFlush(legtipdoc);
        legtipdocSearchRepository.save(legtipdoc);

        // Search the legtipdoc
        restLegtipdocMockMvc.perform(get("/api/_search/legtipdocs?query=id:" + legtipdoc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(legtipdoc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vCodreg").value(hasItem(DEFAULT_V_CODREG.toString())))
            .andExpect(jsonPath("$.[*].vCodzon").value(hasItem(DEFAULT_V_CODZON.toString())))
            .andExpect(jsonPath("$.[*].nCorrel").value(hasItem(DEFAULT_N_CORREL)))
            .andExpect(jsonPath("$.[*].vCodsis").value(hasItem(DEFAULT_V_CODSIS.toString())))
            .andExpect(jsonPath("$.[*].vAsundoc").value(hasItem(DEFAULT_V_ASUNDOC.toString())))
            .andExpect(jsonPath("$.[*].dFecdoc").value(hasItem(DEFAULT_D_FECDOC.toString())))
            .andExpect(jsonPath("$.[*].nMonpag").value(hasItem(DEFAULT_N_MONPAG)))
            .andExpect(jsonPath("$.[*].dFecentr").value(hasItem(DEFAULT_D_FECENTR.toString())))
            .andExpect(jsonPath("$.[*].dFecdev").value(hasItem(DEFAULT_D_FECDEV.toString())))
            .andExpect(jsonPath("$.[*].dFecrecjuz").value(hasItem(DEFAULT_D_FECRECJUZ.toString())))
            .andExpect(jsonPath("$.[*].vCodusureg").value(hasItem(DEFAULT_V_CODUSUREG.toString())))
            .andExpect(jsonPath("$.[*].vHostreg").value(hasItem(DEFAULT_V_HOSTREG.toString())))
            .andExpect(jsonPath("$.[*].vCodusumod").value(hasItem(DEFAULT_V_CODUSUMOD.toString())))
            .andExpect(jsonPath("$.[*].dFecmod").value(hasItem(DEFAULT_D_FECMOD.toString())))
            .andExpect(jsonPath("$.[*].vHostmod").value(hasItem(DEFAULT_V_HOSTMOD.toString())))
            .andExpect(jsonPath("$.[*].dFeccit").value(hasItem(DEFAULT_D_FECCIT.toString())))
            .andExpect(jsonPath("$.[*].vFlgfund").value(hasItem(DEFAULT_V_FLGFUND.toString())))
            .andExpect(jsonPath("$.[*].vFlgasis").value(hasItem(DEFAULT_V_FLGASIS.toString())))
            .andExpect(jsonPath("$.[*].nNumres").value(hasItem(DEFAULT_N_NUMRES)))
            .andExpect(jsonPath("$.[*].nMondol").value(hasItem(DEFAULT_N_MONDOL)))
            .andExpect(jsonPath("$.[*].vCodloc").value(hasItem(DEFAULT_V_CODLOC.toString())))
            .andExpect(jsonPath("$.[*].vFlgconc").value(hasItem(DEFAULT_V_FLGCONC.toString())))
            .andExpect(jsonPath("$.[*].vDettdoc").value(hasItem(DEFAULT_V_DETTDOC.toString())))
            .andExpect(jsonPath("$.[*].dFecdocreq").value(hasItem(DEFAULT_D_FECDOCREQ.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Legtipdoc.class);
        Legtipdoc legtipdoc1 = new Legtipdoc();
        legtipdoc1.setId(1L);
        Legtipdoc legtipdoc2 = new Legtipdoc();
        legtipdoc2.setId(legtipdoc1.getId());
        assertThat(legtipdoc1).isEqualTo(legtipdoc2);
        legtipdoc2.setId(2L);
        assertThat(legtipdoc1).isNotEqualTo(legtipdoc2);
        legtipdoc1.setId(null);
        assertThat(legtipdoc1).isNotEqualTo(legtipdoc2);
    }
}
