package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.PatrocinioApp;

import pe.gob.trabajo.domain.Motfin;
import pe.gob.trabajo.repository.MotfinRepository;
import pe.gob.trabajo.repository.search.MotfinSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MotfinResource REST controller.
 *
 * @see MotfinResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatrocinioApp.class)
public class MotfinResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final String DEFAULT_V_USUAREG = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAREG = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final String DEFAULT_V_USUAUPD = "AAAAAAAAAA";
    private static final String UPDATED_V_USUAUPD = "BBBBBBBBBB";

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private MotfinRepository motfinRepository;

    @Autowired
    private MotfinSearchRepository motfinSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMotfinMockMvc;

    private Motfin motfin;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotfinResource motfinResource = new MotfinResource(motfinRepository, motfinSearchRepository);
        this.restMotfinMockMvc = MockMvcBuilders.standaloneSetup(motfinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Motfin createEntity(EntityManager em) {
        Motfin motfin = new Motfin()
            .vDescrip(DEFAULT_V_DESCRIP)
            .vUsuareg(DEFAULT_V_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .vUsuaupd(DEFAULT_V_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return motfin;
    }

    @Before
    public void initTest() {
        motfinSearchRepository.deleteAll();
        motfin = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotfin() throws Exception {
        int databaseSizeBeforeCreate = motfinRepository.findAll().size();

        // Create the Motfin
        restMotfinMockMvc.perform(post("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isCreated());

        // Validate the Motfin in the database
        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeCreate + 1);
        Motfin testMotfin = motfinList.get(motfinList.size() - 1);
        assertThat(testMotfin.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testMotfin.getvUsuareg()).isEqualTo(DEFAULT_V_USUAREG);
        assertThat(testMotfin.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testMotfin.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testMotfin.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testMotfin.getvUsuaupd()).isEqualTo(DEFAULT_V_USUAUPD);
        assertThat(testMotfin.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testMotfin.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Motfin in Elasticsearch
        Motfin motfinEs = motfinSearchRepository.findOne(testMotfin.getId());
        assertThat(motfinEs).isEqualToComparingFieldByField(testMotfin);
    }

    @Test
    @Transactional
    public void createMotfinWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motfinRepository.findAll().size();

        // Create the Motfin with an existing ID
        motfin.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotfinMockMvc.perform(post("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isBadRequest());

        // Validate the Motfin in the database
        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = motfinRepository.findAll().size();
        // set the field null
        motfin.setvDescrip(null);

        // Create the Motfin, which fails.

        restMotfinMockMvc.perform(post("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isBadRequest());

        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkvUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = motfinRepository.findAll().size();
        // set the field null
        motfin.setvUsuareg(null);

        // Create the Motfin, which fails.

        restMotfinMockMvc.perform(post("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isBadRequest());

        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = motfinRepository.findAll().size();
        // set the field null
        motfin.settFecreg(null);

        // Create the Motfin, which fails.

        restMotfinMockMvc.perform(post("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isBadRequest());

        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = motfinRepository.findAll().size();
        // set the field null
        motfin.setnSedereg(null);

        // Create the Motfin, which fails.

        restMotfinMockMvc.perform(post("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isBadRequest());

        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMotfins() throws Exception {
        // Initialize the database
        motfinRepository.saveAndFlush(motfin);

        // Get all the motfinList
        restMotfinMockMvc.perform(get("/api/motfins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motfin.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getMotfin() throws Exception {
        // Initialize the database
        motfinRepository.saveAndFlush(motfin);

        // Get the motfin
        restMotfinMockMvc.perform(get("/api/motfins/{id}", motfin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motfin.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.vUsuareg").value(DEFAULT_V_USUAREG.toString()))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.vUsuaupd").value(DEFAULT_V_USUAUPD.toString()))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingMotfin() throws Exception {
        // Get the motfin
        restMotfinMockMvc.perform(get("/api/motfins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotfin() throws Exception {
        // Initialize the database
        motfinRepository.saveAndFlush(motfin);
        motfinSearchRepository.save(motfin);
        int databaseSizeBeforeUpdate = motfinRepository.findAll().size();

        // Update the motfin
        Motfin updatedMotfin = motfinRepository.findOne(motfin.getId());
        updatedMotfin
            .vDescrip(UPDATED_V_DESCRIP)
            .vUsuareg(UPDATED_V_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .vUsuaupd(UPDATED_V_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restMotfinMockMvc.perform(put("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMotfin)))
            .andExpect(status().isOk());

        // Validate the Motfin in the database
        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeUpdate);
        Motfin testMotfin = motfinList.get(motfinList.size() - 1);
        assertThat(testMotfin.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testMotfin.getvUsuareg()).isEqualTo(UPDATED_V_USUAREG);
        assertThat(testMotfin.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testMotfin.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testMotfin.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testMotfin.getvUsuaupd()).isEqualTo(UPDATED_V_USUAUPD);
        assertThat(testMotfin.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testMotfin.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Motfin in Elasticsearch
        Motfin motfinEs = motfinSearchRepository.findOne(testMotfin.getId());
        assertThat(motfinEs).isEqualToComparingFieldByField(testMotfin);
    }

    @Test
    @Transactional
    public void updateNonExistingMotfin() throws Exception {
        int databaseSizeBeforeUpdate = motfinRepository.findAll().size();

        // Create the Motfin

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMotfinMockMvc.perform(put("/api/motfins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motfin)))
            .andExpect(status().isCreated());

        // Validate the Motfin in the database
        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMotfin() throws Exception {
        // Initialize the database
        motfinRepository.saveAndFlush(motfin);
        motfinSearchRepository.save(motfin);
        int databaseSizeBeforeDelete = motfinRepository.findAll().size();

        // Get the motfin
        restMotfinMockMvc.perform(delete("/api/motfins/{id}", motfin.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean motfinExistsInEs = motfinSearchRepository.exists(motfin.getId());
        assertThat(motfinExistsInEs).isFalse();

        // Validate the database is empty
        List<Motfin> motfinList = motfinRepository.findAll();
        assertThat(motfinList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMotfin() throws Exception {
        // Initialize the database
        motfinRepository.saveAndFlush(motfin);
        motfinSearchRepository.save(motfin);

        // Search the motfin
        restMotfinMockMvc.perform(get("/api/_search/motfins?query=id:" + motfin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motfin.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].vUsuareg").value(hasItem(DEFAULT_V_USUAREG.toString())))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].vUsuaupd").value(hasItem(DEFAULT_V_USUAUPD.toString())))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Motfin.class);
        Motfin motfin1 = new Motfin();
        motfin1.setId(1L);
        Motfin motfin2 = new Motfin();
        motfin2.setId(motfin1.getId());
        assertThat(motfin1).isEqualTo(motfin2);
        motfin2.setId(2L);
        assertThat(motfin1).isNotEqualTo(motfin2);
        motfin1.setId(null);
        assertThat(motfin1).isNotEqualTo(motfin2);
    }
}
