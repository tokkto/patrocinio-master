package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipdocpj;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipdocpj entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipdocpjRepository extends JpaRepository<Tipdocpj, Long> {

}
