package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Pasepj;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Pasepj entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PasepjRepository extends JpaRepository<Pasepj, Long> {

}
