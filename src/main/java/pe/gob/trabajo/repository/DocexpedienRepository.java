package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Docexpedien;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Docexpedien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocexpedienRepository extends JpaRepository<Docexpedien, Long> {

}
