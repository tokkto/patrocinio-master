package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Datdenu;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Datdenu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DatdenuRepository extends JpaRepository<Datdenu, Long> {

}
