package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Legajoasig;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Legajoasig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LegajoasigRepository extends JpaRepository<Legajoasig, Long> {

}
