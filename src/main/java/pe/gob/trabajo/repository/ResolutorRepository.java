package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Resolutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Resolutor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResolutorRepository extends JpaRepository<Resolutor, Long> {

}
