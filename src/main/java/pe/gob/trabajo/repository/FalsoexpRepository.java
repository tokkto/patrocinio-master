package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Falsoexp;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Falsoexp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FalsoexpRepository extends JpaRepository<Falsoexp, Long> {

}
