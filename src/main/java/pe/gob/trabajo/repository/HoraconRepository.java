package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Horacon;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Horacon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HoraconRepository extends JpaRepository<Horacon, Long> {

}
