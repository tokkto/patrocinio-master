package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Dirdenun;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Dirdenun entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirdenunRepository extends JpaRepository<Dirdenun, Long> {

}
