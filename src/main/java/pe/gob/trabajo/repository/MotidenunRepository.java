package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Motidenun;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Motidenun entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotidenunRepository extends JpaRepository<Motidenun, Long> {

}
