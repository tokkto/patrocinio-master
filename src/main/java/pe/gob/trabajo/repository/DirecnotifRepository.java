package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Direcnotif;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Direcnotif entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirecnotifRepository extends JpaRepository<Direcnotif, Long> {

}
