package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Dirdenun;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Dirdenun entity.
 */
public interface DirdenunSearchRepository extends ElasticsearchRepository<Dirdenun, Long> {
}
