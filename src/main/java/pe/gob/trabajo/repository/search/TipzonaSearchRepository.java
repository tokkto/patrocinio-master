package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipzona;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipzona entity.
 */
public interface TipzonaSearchRepository extends ElasticsearchRepository<Tipzona, Long> {
}
