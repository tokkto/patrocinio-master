package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Pasepj;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Pasepj entity.
 */
public interface PasepjSearchRepository extends ElasticsearchRepository<Pasepj, Long> {
}
