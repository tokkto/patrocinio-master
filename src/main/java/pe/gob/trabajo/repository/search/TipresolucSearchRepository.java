package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipresoluc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipresoluc entity.
 */
public interface TipresolucSearchRepository extends ElasticsearchRepository<Tipresoluc, Long> {
}
