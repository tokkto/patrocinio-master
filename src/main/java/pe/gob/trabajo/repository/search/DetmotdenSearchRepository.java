package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Detmotden;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Detmotden entity.
 */
public interface DetmotdenSearchRepository extends ElasticsearchRepository<Detmotden, Long> {
}
