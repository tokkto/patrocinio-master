package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Denuncia;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Denuncia entity.
 */
public interface DenunciaSearchRepository extends ElasticsearchRepository<Denuncia, Long> {
}
