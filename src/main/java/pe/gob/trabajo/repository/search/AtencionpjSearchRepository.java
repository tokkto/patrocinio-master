package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Atencionpj;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Atencionpj entity.
 */
public interface AtencionpjSearchRepository extends ElasticsearchRepository<Atencionpj, Long> {
}
