package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Legajoasig;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Legajoasig entity.
 */
public interface LegajoasigSearchRepository extends ElasticsearchRepository<Legajoasig, Long> {
}
