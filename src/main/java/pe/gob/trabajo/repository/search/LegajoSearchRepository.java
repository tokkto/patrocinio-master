package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Legajo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Legajo entity.
 */
public interface LegajoSearchRepository extends ElasticsearchRepository<Legajo, Long> {
}
