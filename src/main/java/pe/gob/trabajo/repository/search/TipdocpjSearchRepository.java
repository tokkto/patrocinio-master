package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipdocpj;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipdocpj entity.
 */
public interface TipdocpjSearchRepository extends ElasticsearchRepository<Tipdocpj, Long> {
}
