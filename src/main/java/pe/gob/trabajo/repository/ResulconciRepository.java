package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Resulconci;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Resulconci entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResulconciRepository extends JpaRepository<Resulconci, Long> {

}
