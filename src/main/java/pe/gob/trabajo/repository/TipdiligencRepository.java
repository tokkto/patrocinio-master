package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipdiligenc;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipdiligenc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipdiligencRepository extends JpaRepository<Tipdiligenc, Long> {

}
