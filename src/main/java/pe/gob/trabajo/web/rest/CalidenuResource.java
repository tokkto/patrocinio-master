package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Calidenu;

import pe.gob.trabajo.repository.CalidenuRepository;
import pe.gob.trabajo.repository.search.CalidenuSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Calidenu.
 */
@RestController
@RequestMapping("/api")
public class CalidenuResource {

    private final Logger log = LoggerFactory.getLogger(CalidenuResource.class);

    private static final String ENTITY_NAME = "calidenu";

    private final CalidenuRepository calidenuRepository;

    private final CalidenuSearchRepository calidenuSearchRepository;

    public CalidenuResource(CalidenuRepository calidenuRepository, CalidenuSearchRepository calidenuSearchRepository) {
        this.calidenuRepository = calidenuRepository;
        this.calidenuSearchRepository = calidenuSearchRepository;
    }

    /**
     * POST  /calidenus : Create a new calidenu.
     *
     * @param calidenu the calidenu to create
     * @return the ResponseEntity with status 201 (Created) and with body the new calidenu, or with status 400 (Bad Request) if the calidenu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/calidenus")
    @Timed
    public ResponseEntity<Calidenu> createCalidenu(@Valid @RequestBody Calidenu calidenu) throws URISyntaxException {
        log.debug("REST request to save Calidenu : {}", calidenu);
        if (calidenu.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new calidenu cannot already have an ID")).body(null);
        }
        Calidenu result = calidenuRepository.save(calidenu);
        calidenuSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/calidenus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /calidenus : Updates an existing calidenu.
     *
     * @param calidenu the calidenu to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated calidenu,
     * or with status 400 (Bad Request) if the calidenu is not valid,
     * or with status 500 (Internal Server Error) if the calidenu couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/calidenus")
    @Timed
    public ResponseEntity<Calidenu> updateCalidenu(@Valid @RequestBody Calidenu calidenu) throws URISyntaxException {
        log.debug("REST request to update Calidenu : {}", calidenu);
        if (calidenu.getId() == null) {
            return createCalidenu(calidenu);
        }
        Calidenu result = calidenuRepository.save(calidenu);
        calidenuSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, calidenu.getId().toString()))
            .body(result);
    }

    /**
     * GET  /calidenus : get all the calidenus.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of calidenus in body
     */
    @GetMapping("/calidenus")
    @Timed
    public List<Calidenu> getAllCalidenus() {
        log.debug("REST request to get all Calidenus");
        return calidenuRepository.findAll();
        }

    /**
     * GET  /calidenus/:id : get the "id" calidenu.
     *
     * @param id the id of the calidenu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the calidenu, or with status 404 (Not Found)
     */
    @GetMapping("/calidenus/{id}")
    @Timed
    public ResponseEntity<Calidenu> getCalidenu(@PathVariable Long id) {
        log.debug("REST request to get Calidenu : {}", id);
        Calidenu calidenu = calidenuRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(calidenu));
    }

    /**
     * DELETE  /calidenus/:id : delete the "id" calidenu.
     *
     * @param id the id of the calidenu to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/calidenus/{id}")
    @Timed
    public ResponseEntity<Void> deleteCalidenu(@PathVariable Long id) {
        log.debug("REST request to delete Calidenu : {}", id);
        calidenuRepository.delete(id);
        calidenuSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/calidenus?query=:query : search for the calidenu corresponding
     * to the query.
     *
     * @param query the query of the calidenu search
     * @return the result of the search
     */
    @GetMapping("/_search/calidenus")
    @Timed
    public List<Calidenu> searchCalidenus(@RequestParam String query) {
        log.debug("REST request to search Calidenus for query {}", query);
        return StreamSupport
            .stream(calidenuSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
