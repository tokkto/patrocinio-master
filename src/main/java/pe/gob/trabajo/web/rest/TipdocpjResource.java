package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipdocpj;

import pe.gob.trabajo.repository.TipdocpjRepository;
import pe.gob.trabajo.repository.search.TipdocpjSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipdocpj.
 */
@RestController
@RequestMapping("/api")
public class TipdocpjResource {

    private final Logger log = LoggerFactory.getLogger(TipdocpjResource.class);

    private static final String ENTITY_NAME = "tipdocpj";

    private final TipdocpjRepository tipdocpjRepository;

    private final TipdocpjSearchRepository tipdocpjSearchRepository;

    public TipdocpjResource(TipdocpjRepository tipdocpjRepository, TipdocpjSearchRepository tipdocpjSearchRepository) {
        this.tipdocpjRepository = tipdocpjRepository;
        this.tipdocpjSearchRepository = tipdocpjSearchRepository;
    }

    /**
     * POST  /tipdocpjs : Create a new tipdocpj.
     *
     * @param tipdocpj the tipdocpj to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipdocpj, or with status 400 (Bad Request) if the tipdocpj has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipdocpjs")
    @Timed
    public ResponseEntity<Tipdocpj> createTipdocpj(@Valid @RequestBody Tipdocpj tipdocpj) throws URISyntaxException {
        log.debug("REST request to save Tipdocpj : {}", tipdocpj);
        if (tipdocpj.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipdocpj cannot already have an ID")).body(null);
        }
        Tipdocpj result = tipdocpjRepository.save(tipdocpj);
        tipdocpjSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipdocpjs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipdocpjs : Updates an existing tipdocpj.
     *
     * @param tipdocpj the tipdocpj to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipdocpj,
     * or with status 400 (Bad Request) if the tipdocpj is not valid,
     * or with status 500 (Internal Server Error) if the tipdocpj couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipdocpjs")
    @Timed
    public ResponseEntity<Tipdocpj> updateTipdocpj(@Valid @RequestBody Tipdocpj tipdocpj) throws URISyntaxException {
        log.debug("REST request to update Tipdocpj : {}", tipdocpj);
        if (tipdocpj.getId() == null) {
            return createTipdocpj(tipdocpj);
        }
        Tipdocpj result = tipdocpjRepository.save(tipdocpj);
        tipdocpjSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipdocpj.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipdocpjs : get all the tipdocpjs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipdocpjs in body
     */
    @GetMapping("/tipdocpjs")
    @Timed
    public List<Tipdocpj> getAllTipdocpjs() {
        log.debug("REST request to get all Tipdocpjs");
        return tipdocpjRepository.findAll();
        }

    /**
     * GET  /tipdocpjs/:id : get the "id" tipdocpj.
     *
     * @param id the id of the tipdocpj to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipdocpj, or with status 404 (Not Found)
     */
    @GetMapping("/tipdocpjs/{id}")
    @Timed
    public ResponseEntity<Tipdocpj> getTipdocpj(@PathVariable Long id) {
        log.debug("REST request to get Tipdocpj : {}", id);
        Tipdocpj tipdocpj = tipdocpjRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipdocpj));
    }

    /**
     * DELETE  /tipdocpjs/:id : delete the "id" tipdocpj.
     *
     * @param id the id of the tipdocpj to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipdocpjs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipdocpj(@PathVariable Long id) {
        log.debug("REST request to delete Tipdocpj : {}", id);
        tipdocpjRepository.delete(id);
        tipdocpjSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipdocpjs?query=:query : search for the tipdocpj corresponding
     * to the query.
     *
     * @param query the query of the tipdocpj search
     * @return the result of the search
     */
    @GetMapping("/_search/tipdocpjs")
    @Timed
    public List<Tipdocpj> searchTipdocpjs(@RequestParam String query) {
        log.debug("REST request to search Tipdocpjs for query {}", query);
        return StreamSupport
            .stream(tipdocpjSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
