package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Legajoasig;

import pe.gob.trabajo.repository.LegajoasigRepository;
import pe.gob.trabajo.repository.search.LegajoasigSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Legajoasig.
 */
@RestController
@RequestMapping("/api")
public class LegajoasigResource {

    private final Logger log = LoggerFactory.getLogger(LegajoasigResource.class);

    private static final String ENTITY_NAME = "legajoasig";

    private final LegajoasigRepository legajoasigRepository;

    private final LegajoasigSearchRepository legajoasigSearchRepository;

    public LegajoasigResource(LegajoasigRepository legajoasigRepository, LegajoasigSearchRepository legajoasigSearchRepository) {
        this.legajoasigRepository = legajoasigRepository;
        this.legajoasigSearchRepository = legajoasigSearchRepository;
    }

    /**
     * POST  /legajoasigs : Create a new legajoasig.
     *
     * @param legajoasig the legajoasig to create
     * @return the ResponseEntity with status 201 (Created) and with body the new legajoasig, or with status 400 (Bad Request) if the legajoasig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/legajoasigs")
    @Timed
    public ResponseEntity<Legajoasig> createLegajoasig(@Valid @RequestBody Legajoasig legajoasig) throws URISyntaxException {
        log.debug("REST request to save Legajoasig : {}", legajoasig);
        if (legajoasig.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new legajoasig cannot already have an ID")).body(null);
        }
        Legajoasig result = legajoasigRepository.save(legajoasig);
        legajoasigSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/legajoasigs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /legajoasigs : Updates an existing legajoasig.
     *
     * @param legajoasig the legajoasig to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated legajoasig,
     * or with status 400 (Bad Request) if the legajoasig is not valid,
     * or with status 500 (Internal Server Error) if the legajoasig couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/legajoasigs")
    @Timed
    public ResponseEntity<Legajoasig> updateLegajoasig(@Valid @RequestBody Legajoasig legajoasig) throws URISyntaxException {
        log.debug("REST request to update Legajoasig : {}", legajoasig);
        if (legajoasig.getId() == null) {
            return createLegajoasig(legajoasig);
        }
        Legajoasig result = legajoasigRepository.save(legajoasig);
        legajoasigSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, legajoasig.getId().toString()))
            .body(result);
    }

    /**
     * GET  /legajoasigs : get all the legajoasigs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of legajoasigs in body
     */
    @GetMapping("/legajoasigs")
    @Timed
    public List<Legajoasig> getAllLegajoasigs() {
        log.debug("REST request to get all Legajoasigs");
        return legajoasigRepository.findAll();
        }

    /**
     * GET  /legajoasigs/:id : get the "id" legajoasig.
     *
     * @param id the id of the legajoasig to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the legajoasig, or with status 404 (Not Found)
     */
    @GetMapping("/legajoasigs/{id}")
    @Timed
    public ResponseEntity<Legajoasig> getLegajoasig(@PathVariable Long id) {
        log.debug("REST request to get Legajoasig : {}", id);
        Legajoasig legajoasig = legajoasigRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(legajoasig));
    }

    /**
     * DELETE  /legajoasigs/:id : delete the "id" legajoasig.
     *
     * @param id the id of the legajoasig to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/legajoasigs/{id}")
    @Timed
    public ResponseEntity<Void> deleteLegajoasig(@PathVariable Long id) {
        log.debug("REST request to delete Legajoasig : {}", id);
        legajoasigRepository.delete(id);
        legajoasigSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/legajoasigs?query=:query : search for the legajoasig corresponding
     * to the query.
     *
     * @param query the query of the legajoasig search
     * @return the result of the search
     */
    @GetMapping("/_search/legajoasigs")
    @Timed
    public List<Legajoasig> searchLegajoasigs(@RequestParam String query) {
        log.debug("REST request to search Legajoasigs for query {}", query);
        return StreamSupport
            .stream(legajoasigSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
