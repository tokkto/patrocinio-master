package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipzona;

import pe.gob.trabajo.repository.TipzonaRepository;
import pe.gob.trabajo.repository.search.TipzonaSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipzona.
 */
@RestController
@RequestMapping("/api")
public class TipzonaResource {

    private final Logger log = LoggerFactory.getLogger(TipzonaResource.class);

    private static final String ENTITY_NAME = "tipzona";

    private final TipzonaRepository tipzonaRepository;

    private final TipzonaSearchRepository tipzonaSearchRepository;

    public TipzonaResource(TipzonaRepository tipzonaRepository, TipzonaSearchRepository tipzonaSearchRepository) {
        this.tipzonaRepository = tipzonaRepository;
        this.tipzonaSearchRepository = tipzonaSearchRepository;
    }

    /**
     * POST  /tipzonas : Create a new tipzona.
     *
     * @param tipzona the tipzona to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipzona, or with status 400 (Bad Request) if the tipzona has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipzonas")
    @Timed
    public ResponseEntity<Tipzona> createTipzona(@Valid @RequestBody Tipzona tipzona) throws URISyntaxException {
        log.debug("REST request to save Tipzona : {}", tipzona);
        if (tipzona.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipzona cannot already have an ID")).body(null);
        }
        Tipzona result = tipzonaRepository.save(tipzona);
        tipzonaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipzonas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipzonas : Updates an existing tipzona.
     *
     * @param tipzona the tipzona to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipzona,
     * or with status 400 (Bad Request) if the tipzona is not valid,
     * or with status 500 (Internal Server Error) if the tipzona couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipzonas")
    @Timed
    public ResponseEntity<Tipzona> updateTipzona(@Valid @RequestBody Tipzona tipzona) throws URISyntaxException {
        log.debug("REST request to update Tipzona : {}", tipzona);
        if (tipzona.getId() == null) {
            return createTipzona(tipzona);
        }
        Tipzona result = tipzonaRepository.save(tipzona);
        tipzonaSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipzona.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipzonas : get all the tipzonas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipzonas in body
     */
    @GetMapping("/tipzonas")
    @Timed
    public List<Tipzona> getAllTipzonas() {
        log.debug("REST request to get all Tipzonas");
        return tipzonaRepository.findAll();
        }

    /**
     * GET  /tipzonas/:id : get the "id" tipzona.
     *
     * @param id the id of the tipzona to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipzona, or with status 404 (Not Found)
     */
    @GetMapping("/tipzonas/{id}")
    @Timed
    public ResponseEntity<Tipzona> getTipzona(@PathVariable Long id) {
        log.debug("REST request to get Tipzona : {}", id);
        Tipzona tipzona = tipzonaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipzona));
    }

    /**
     * DELETE  /tipzonas/:id : delete the "id" tipzona.
     *
     * @param id the id of the tipzona to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipzonas/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipzona(@PathVariable Long id) {
        log.debug("REST request to delete Tipzona : {}", id);
        tipzonaRepository.delete(id);
        tipzonaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipzonas?query=:query : search for the tipzona corresponding
     * to the query.
     *
     * @param query the query of the tipzona search
     * @return the result of the search
     */
    @GetMapping("/_search/tipzonas")
    @Timed
    public List<Tipzona> searchTipzonas(@RequestParam String query) {
        log.debug("REST request to search Tipzonas for query {}", query);
        return StreamSupport
            .stream(tipzonaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
