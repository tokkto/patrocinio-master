package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Datdenu;

import pe.gob.trabajo.repository.DatdenuRepository;
import pe.gob.trabajo.repository.search.DatdenuSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Datdenu.
 */
@RestController
@RequestMapping("/api")
public class DatdenuResource {

    private final Logger log = LoggerFactory.getLogger(DatdenuResource.class);

    private static final String ENTITY_NAME = "datdenu";

    private final DatdenuRepository datdenuRepository;

    private final DatdenuSearchRepository datdenuSearchRepository;

    public DatdenuResource(DatdenuRepository datdenuRepository, DatdenuSearchRepository datdenuSearchRepository) {
        this.datdenuRepository = datdenuRepository;
        this.datdenuSearchRepository = datdenuSearchRepository;
    }

    /**
     * POST  /datdenus : Create a new datdenu.
     *
     * @param datdenu the datdenu to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datdenu, or with status 400 (Bad Request) if the datdenu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/datdenus")
    @Timed
    public ResponseEntity<Datdenu> createDatdenu(@Valid @RequestBody Datdenu datdenu) throws URISyntaxException {
        log.debug("REST request to save Datdenu : {}", datdenu);
        if (datdenu.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new datdenu cannot already have an ID")).body(null);
        }
        Datdenu result = datdenuRepository.save(datdenu);
        datdenuSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/datdenus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /datdenus : Updates an existing datdenu.
     *
     * @param datdenu the datdenu to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datdenu,
     * or with status 400 (Bad Request) if the datdenu is not valid,
     * or with status 500 (Internal Server Error) if the datdenu couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/datdenus")
    @Timed
    public ResponseEntity<Datdenu> updateDatdenu(@Valid @RequestBody Datdenu datdenu) throws URISyntaxException {
        log.debug("REST request to update Datdenu : {}", datdenu);
        if (datdenu.getId() == null) {
            return createDatdenu(datdenu);
        }
        Datdenu result = datdenuRepository.save(datdenu);
        datdenuSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, datdenu.getId().toString()))
            .body(result);
    }

    /**
     * GET  /datdenus : get all the datdenus.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of datdenus in body
     */
    @GetMapping("/datdenus")
    @Timed
    public List<Datdenu> getAllDatdenus() {
        log.debug("REST request to get all Datdenus");
        return datdenuRepository.findAll();
        }

    /**
     * GET  /datdenus/:id : get the "id" datdenu.
     *
     * @param id the id of the datdenu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the datdenu, or with status 404 (Not Found)
     */
    @GetMapping("/datdenus/{id}")
    @Timed
    public ResponseEntity<Datdenu> getDatdenu(@PathVariable Long id) {
        log.debug("REST request to get Datdenu : {}", id);
        Datdenu datdenu = datdenuRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(datdenu));
    }

    /**
     * DELETE  /datdenus/:id : delete the "id" datdenu.
     *
     * @param id the id of the datdenu to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/datdenus/{id}")
    @Timed
    public ResponseEntity<Void> deleteDatdenu(@PathVariable Long id) {
        log.debug("REST request to delete Datdenu : {}", id);
        datdenuRepository.delete(id);
        datdenuSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/datdenus?query=:query : search for the datdenu corresponding
     * to the query.
     *
     * @param query the query of the datdenu search
     * @return the result of the search
     */
    @GetMapping("/_search/datdenus")
    @Timed
    public List<Datdenu> searchDatdenus(@RequestParam String query) {
        log.debug("REST request to search Datdenus for query {}", query);
        return StreamSupport
            .stream(datdenuSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
