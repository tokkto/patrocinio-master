package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Califica;

import pe.gob.trabajo.repository.CalificaRepository;
import pe.gob.trabajo.repository.search.CalificaSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Califica.
 */
@RestController
@RequestMapping("/api")
public class CalificaResource {

    private final Logger log = LoggerFactory.getLogger(CalificaResource.class);

    private static final String ENTITY_NAME = "califica";

    private final CalificaRepository calificaRepository;

    private final CalificaSearchRepository calificaSearchRepository;

    public CalificaResource(CalificaRepository calificaRepository, CalificaSearchRepository calificaSearchRepository) {
        this.calificaRepository = calificaRepository;
        this.calificaSearchRepository = calificaSearchRepository;
    }

    /**
     * POST  /calificas : Create a new califica.
     *
     * @param califica the califica to create
     * @return the ResponseEntity with status 201 (Created) and with body the new califica, or with status 400 (Bad Request) if the califica has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/calificas")
    @Timed
    public ResponseEntity<Califica> createCalifica(@Valid @RequestBody Califica califica) throws URISyntaxException {
        log.debug("REST request to save Califica : {}", califica);
        if (califica.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new califica cannot already have an ID")).body(null);
        }
        Califica result = calificaRepository.save(califica);
        calificaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/calificas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /calificas : Updates an existing califica.
     *
     * @param califica the califica to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated califica,
     * or with status 400 (Bad Request) if the califica is not valid,
     * or with status 500 (Internal Server Error) if the califica couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/calificas")
    @Timed
    public ResponseEntity<Califica> updateCalifica(@Valid @RequestBody Califica califica) throws URISyntaxException {
        log.debug("REST request to update Califica : {}", califica);
        if (califica.getId() == null) {
            return createCalifica(califica);
        }
        Califica result = calificaRepository.save(califica);
        calificaSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, califica.getId().toString()))
            .body(result);
    }

    /**
     * GET  /calificas : get all the calificas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of calificas in body
     */
    @GetMapping("/calificas")
    @Timed
    public List<Califica> getAllCalificas() {
        log.debug("REST request to get all Calificas");
        return calificaRepository.findAll();
        }

    /**
     * GET  /calificas/:id : get the "id" califica.
     *
     * @param id the id of the califica to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the califica, or with status 404 (Not Found)
     */
    @GetMapping("/calificas/{id}")
    @Timed
    public ResponseEntity<Califica> getCalifica(@PathVariable Long id) {
        log.debug("REST request to get Califica : {}", id);
        Califica califica = calificaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(califica));
    }

    /**
     * DELETE  /calificas/:id : delete the "id" califica.
     *
     * @param id the id of the califica to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/calificas/{id}")
    @Timed
    public ResponseEntity<Void> deleteCalifica(@PathVariable Long id) {
        log.debug("REST request to delete Califica : {}", id);
        calificaRepository.delete(id);
        calificaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/calificas?query=:query : search for the califica corresponding
     * to the query.
     *
     * @param query the query of the califica search
     * @return the result of the search
     */
    @GetMapping("/_search/calificas")
    @Timed
    public List<Califica> searchCalificas(@RequestParam String query) {
        log.debug("REST request to search Calificas for query {}", query);
        return StreamSupport
            .stream(calificaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
