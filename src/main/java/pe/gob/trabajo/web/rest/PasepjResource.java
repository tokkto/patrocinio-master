package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Pasepj;

import pe.gob.trabajo.repository.PasepjRepository;
import pe.gob.trabajo.repository.search.PasepjSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pasepj.
 */
@RestController
@RequestMapping("/api")
public class PasepjResource {

    private final Logger log = LoggerFactory.getLogger(PasepjResource.class);

    private static final String ENTITY_NAME = "pasepj";

    private final PasepjRepository pasepjRepository;

    private final PasepjSearchRepository pasepjSearchRepository;

    public PasepjResource(PasepjRepository pasepjRepository, PasepjSearchRepository pasepjSearchRepository) {
        this.pasepjRepository = pasepjRepository;
        this.pasepjSearchRepository = pasepjSearchRepository;
    }

    /**
     * POST  /pasepjs : Create a new pasepj.
     *
     * @param pasepj the pasepj to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pasepj, or with status 400 (Bad Request) if the pasepj has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pasepjs")
    @Timed
    public ResponseEntity<Pasepj> createPasepj(@Valid @RequestBody Pasepj pasepj) throws URISyntaxException {
        log.debug("REST request to save Pasepj : {}", pasepj);
        if (pasepj.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pasepj cannot already have an ID")).body(null);
        }
        Pasepj result = pasepjRepository.save(pasepj);
        pasepjSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pasepjs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pasepjs : Updates an existing pasepj.
     *
     * @param pasepj the pasepj to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pasepj,
     * or with status 400 (Bad Request) if the pasepj is not valid,
     * or with status 500 (Internal Server Error) if the pasepj couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pasepjs")
    @Timed
    public ResponseEntity<Pasepj> updatePasepj(@Valid @RequestBody Pasepj pasepj) throws URISyntaxException {
        log.debug("REST request to update Pasepj : {}", pasepj);
        if (pasepj.getId() == null) {
            return createPasepj(pasepj);
        }
        Pasepj result = pasepjRepository.save(pasepj);
        pasepjSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pasepj.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pasepjs : get all the pasepjs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pasepjs in body
     */
    @GetMapping("/pasepjs")
    @Timed
    public List<Pasepj> getAllPasepjs() {
        log.debug("REST request to get all Pasepjs");
        return pasepjRepository.findAll();
        }

    /**
     * GET  /pasepjs/:id : get the "id" pasepj.
     *
     * @param id the id of the pasepj to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pasepj, or with status 404 (Not Found)
     */
    @GetMapping("/pasepjs/{id}")
    @Timed
    public ResponseEntity<Pasepj> getPasepj(@PathVariable Long id) {
        log.debug("REST request to get Pasepj : {}", id);
        Pasepj pasepj = pasepjRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pasepj));
    }

    /**
     * DELETE  /pasepjs/:id : delete the "id" pasepj.
     *
     * @param id the id of the pasepj to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pasepjs/{id}")
    @Timed
    public ResponseEntity<Void> deletePasepj(@PathVariable Long id) {
        log.debug("REST request to delete Pasepj : {}", id);
        pasepjRepository.delete(id);
        pasepjSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/pasepjs?query=:query : search for the pasepj corresponding
     * to the query.
     *
     * @param query the query of the pasepj search
     * @return the result of the search
     */
    @GetMapping("/_search/pasepjs")
    @Timed
    public List<Pasepj> searchPasepjs(@RequestParam String query) {
        log.debug("REST request to search Pasepjs for query {}", query);
        return StreamSupport
            .stream(pasepjSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
