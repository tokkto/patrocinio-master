package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Denunte;

import pe.gob.trabajo.repository.DenunteRepository;
import pe.gob.trabajo.repository.search.DenunteSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Denunte.
 */
@RestController
@RequestMapping("/api")
public class DenunteResource {

    private final Logger log = LoggerFactory.getLogger(DenunteResource.class);

    private static final String ENTITY_NAME = "denunte";

    private final DenunteRepository denunteRepository;

    private final DenunteSearchRepository denunteSearchRepository;

    public DenunteResource(DenunteRepository denunteRepository, DenunteSearchRepository denunteSearchRepository) {
        this.denunteRepository = denunteRepository;
        this.denunteSearchRepository = denunteSearchRepository;
    }

    /**
     * POST  /denuntes : Create a new denunte.
     *
     * @param denunte the denunte to create
     * @return the ResponseEntity with status 201 (Created) and with body the new denunte, or with status 400 (Bad Request) if the denunte has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/denuntes")
    @Timed
    public ResponseEntity<Denunte> createDenunte(@Valid @RequestBody Denunte denunte) throws URISyntaxException {
        log.debug("REST request to save Denunte : {}", denunte);
        if (denunte.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new denunte cannot already have an ID")).body(null);
        }
        Denunte result = denunteRepository.save(denunte);
        denunteSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/denuntes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /denuntes : Updates an existing denunte.
     *
     * @param denunte the denunte to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated denunte,
     * or with status 400 (Bad Request) if the denunte is not valid,
     * or with status 500 (Internal Server Error) if the denunte couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/denuntes")
    @Timed
    public ResponseEntity<Denunte> updateDenunte(@Valid @RequestBody Denunte denunte) throws URISyntaxException {
        log.debug("REST request to update Denunte : {}", denunte);
        if (denunte.getId() == null) {
            return createDenunte(denunte);
        }
        Denunte result = denunteRepository.save(denunte);
        denunteSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, denunte.getId().toString()))
            .body(result);
    }

    /**
     * GET  /denuntes : get all the denuntes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of denuntes in body
     */
    @GetMapping("/denuntes")
    @Timed
    public List<Denunte> getAllDenuntes() {
        log.debug("REST request to get all Denuntes");
        return denunteRepository.findAll();
        }

    /**
     * GET  /denuntes/:id : get the "id" denunte.
     *
     * @param id the id of the denunte to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the denunte, or with status 404 (Not Found)
     */
    @GetMapping("/denuntes/{id}")
    @Timed
    public ResponseEntity<Denunte> getDenunte(@PathVariable Long id) {
        log.debug("REST request to get Denunte : {}", id);
        Denunte denunte = denunteRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(denunte));
    }

    /**
     * DELETE  /denuntes/:id : delete the "id" denunte.
     *
     * @param id the id of the denunte to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/denuntes/{id}")
    @Timed
    public ResponseEntity<Void> deleteDenunte(@PathVariable Long id) {
        log.debug("REST request to delete Denunte : {}", id);
        denunteRepository.delete(id);
        denunteSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/denuntes?query=:query : search for the denunte corresponding
     * to the query.
     *
     * @param query the query of the denunte search
     * @return the result of the search
     */
    @GetMapping("/_search/denuntes")
    @Timed
    public List<Denunte> searchDenuntes(@RequestParam String query) {
        log.debug("REST request to search Denuntes for query {}", query);
        return StreamSupport
            .stream(denunteSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
