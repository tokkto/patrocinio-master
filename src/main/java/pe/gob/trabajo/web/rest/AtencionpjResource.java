package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Atencionpj;

import pe.gob.trabajo.repository.AtencionpjRepository;
import pe.gob.trabajo.repository.search.AtencionpjSearchRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Atencionpj.
 */
@RestController
@RequestMapping("/api")
public class AtencionpjResource {

    private final Logger log = LoggerFactory.getLogger(AtencionpjResource.class);

    private static final String ENTITY_NAME = "atencionpj";

    private final AtencionpjRepository atencionpjRepository;

    private final AtencionpjSearchRepository atencionpjSearchRepository;

    public AtencionpjResource(AtencionpjRepository atencionpjRepository, AtencionpjSearchRepository atencionpjSearchRepository) {
        this.atencionpjRepository = atencionpjRepository;
        this.atencionpjSearchRepository = atencionpjSearchRepository;
    }

    /**
     * POST  /atencionpjs : Create a new atencionpj.
     *
     * @param atencionpj the atencionpj to create
     * @return the ResponseEntity with status 201 (Created) and with body the new atencionpj, or with status 400 (Bad Request) if the atencionpj has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/atencionpjs")
    @Timed
    public ResponseEntity<Atencionpj> createAtencionpj(@Valid @RequestBody Atencionpj atencionpj) throws URISyntaxException {
        log.debug("REST request to save Atencionpj : {}", atencionpj);
        if (atencionpj.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new atencionpj cannot already have an ID")).body(null);
        }
        Atencionpj result = atencionpjRepository.save(atencionpj);
        atencionpjSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/atencionpjs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /atencionpjs : Updates an existing atencionpj.
     *
     * @param atencionpj the atencionpj to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated atencionpj,
     * or with status 400 (Bad Request) if the atencionpj is not valid,
     * or with status 500 (Internal Server Error) if the atencionpj couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/atencionpjs")
    @Timed
    public ResponseEntity<Atencionpj> updateAtencionpj(@Valid @RequestBody Atencionpj atencionpj) throws URISyntaxException {
        log.debug("REST request to update Atencionpj : {}", atencionpj);
        if (atencionpj.getId() == null) {
            return createAtencionpj(atencionpj);
        }
        Atencionpj result = atencionpjRepository.save(atencionpj);
        atencionpjSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, atencionpj.getId().toString()))
            .body(result);
    }

    /**
     * GET  /atencionpjs : get all the atencionpjs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of atencionpjs in body
     */
    @GetMapping("/atencionpjs")
    @Timed
    public List<Atencionpj> getAllAtencionpjs() {
        log.debug("REST request to get all Atencionpjs");
        return atencionpjRepository.findAll();
        }

    /**
     * GET  /atencionpjs/:id : get the "id" atencionpj.
     *
     * @param id the id of the atencionpj to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the atencionpj, or with status 404 (Not Found)
     */
    @GetMapping("/atencionpjs/{id}")
    @Timed
    public ResponseEntity<Atencionpj> getAtencionpj(@PathVariable Long id) {
        log.debug("REST request to get Atencionpj : {}", id);
        Atencionpj atencionpj = atencionpjRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(atencionpj));
    }

    /**
     * DELETE  /atencionpjs/:id : delete the "id" atencionpj.
     *
     * @param id the id of the atencionpj to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/atencionpjs/{id}")
    @Timed
    public ResponseEntity<Void> deleteAtencionpj(@PathVariable Long id) {
        log.debug("REST request to delete Atencionpj : {}", id);
        atencionpjRepository.delete(id);
        atencionpjSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/atencionpjs?query=:query : search for the atencionpj corresponding
     * to the query.
     *
     * @param query the query of the atencionpj search
     * @return the result of the search
     */
    @GetMapping("/_search/atencionpjs")
    @Timed
    public List<Atencionpj> searchAtencionpjs(@RequestParam String query) {
        log.debug("REST request to search Atencionpjs for query {}", query);
        return StreamSupport
            .stream(atencionpjSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
