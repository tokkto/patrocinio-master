package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tipdocpj.
 */
@Entity
@Table(name = "pjtbd_tipdocpj")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjtbd_tipdocpj")
public class Tipdocpj implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codtdoc", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 3)
    @Column(name = "v_codsis", length = 3, nullable = false)
    private String vCodsis;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_destdoc", length = 100, nullable = false)
    private String vDestdoc;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgtip", length = 1, nullable = false)
    private String vFlgtip;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_flgres", length = 1, nullable = false)
    private String vFlgres;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "tipdocpj")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Legtipdoc> legtipdocs = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodsis() {
        return vCodsis;
    }

    public Tipdocpj vCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
        return this;
    }

    public void setvCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
    }

    public String getvDestdoc() {
        return vDestdoc;
    }

    public Tipdocpj vDestdoc(String vDestdoc) {
        this.vDestdoc = vDestdoc;
        return this;
    }

    public void setvDestdoc(String vDestdoc) {
        this.vDestdoc = vDestdoc;
    }

    public String getvFlgtip() {
        return vFlgtip;
    }

    public Tipdocpj vFlgtip(String vFlgtip) {
        this.vFlgtip = vFlgtip;
        return this;
    }

    public void setvFlgtip(String vFlgtip) {
        this.vFlgtip = vFlgtip;
    }

    public String getvFlgres() {
        return vFlgres;
    }

    public Tipdocpj vFlgres(String vFlgres) {
        this.vFlgres = vFlgres;
        return this;
    }

    public void setvFlgres(String vFlgres) {
        this.vFlgres = vFlgres;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Tipdocpj nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Tipdocpj tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Tipdocpj nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Tipdocpj nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Tipdocpj nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Tipdocpj tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Tipdocpj nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Legtipdoc> getLegtipdocs() {
        return legtipdocs;
    }

    public Tipdocpj legtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
        return this;
    }

    public Tipdocpj addLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.add(legtipdoc);
        legtipdoc.setTipdocpj(this);
        return this;
    }

    public Tipdocpj removeLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.remove(legtipdoc);
        legtipdoc.setTipdocpj(null);
        return this;
    }

    public void setLegtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tipdocpj tipdocpj = (Tipdocpj) o;
        if (tipdocpj.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipdocpj.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tipdocpj{" +
            "id=" + getId() +
            ", vCodsis='" + getvCodsis() + "'" +
            ", vDestdoc='" + getvDestdoc() + "'" +
            ", vFlgtip='" + getvFlgtip() + "'" +
            ", vFlgres='" + getvFlgres() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
