package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Infosoli.
 */
@Entity
@Table(name = "infosoli")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "infosoli")
public class Infosoli implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 300)
    @Column(name = "v_infosoli", length = 300, nullable = false)
    private String vInfosoli;

    @NotNull
    @Column(name = "t_fecsoli", nullable = false)
    private Instant tFecsoli;

    @Size(max = 300)
    @Column(name = "v_respuesta", length = 300)
    private String vRespuesta;

    @Column(name = "t_fecresp")
    private Instant tFecresp;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_usuareg", length = 20, nullable = false)
    private String vUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @Column(name = "n_flgactivo")
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Size(max = 20)
    @Column(name = "v_usuaupd", length = 20)
    private String vUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "infosoli")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Denuncia> denuncias = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvInfosoli() {
        return vInfosoli;
    }

    public Infosoli vInfosoli(String vInfosoli) {
        this.vInfosoli = vInfosoli;
        return this;
    }

    public void setvInfosoli(String vInfosoli) {
        this.vInfosoli = vInfosoli;
    }

    public Instant gettFecsoli() {
        return tFecsoli;
    }

    public Infosoli tFecsoli(Instant tFecsoli) {
        this.tFecsoli = tFecsoli;
        return this;
    }

    public void settFecsoli(Instant tFecsoli) {
        this.tFecsoli = tFecsoli;
    }

    public String getvRespuesta() {
        return vRespuesta;
    }

    public Infosoli vRespuesta(String vRespuesta) {
        this.vRespuesta = vRespuesta;
        return this;
    }

    public void setvRespuesta(String vRespuesta) {
        this.vRespuesta = vRespuesta;
    }

    public Instant gettFecresp() {
        return tFecresp;
    }

    public Infosoli tFecresp(Instant tFecresp) {
        this.tFecresp = tFecresp;
        return this;
    }

    public void settFecresp(Instant tFecresp) {
        this.tFecresp = tFecresp;
    }

    public String getvUsuareg() {
        return vUsuareg;
    }

    public Infosoli vUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
        return this;
    }

    public void setvUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Infosoli tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Infosoli nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Infosoli nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public String getvUsuaupd() {
        return vUsuaupd;
    }

    public Infosoli vUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
        return this;
    }

    public void setvUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Infosoli tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Infosoli nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Denuncia> getDenuncias() {
        return denuncias;
    }

    public Infosoli denuncias(Set<Denuncia> denuncias) {
        this.denuncias = denuncias;
        return this;
    }

    public Infosoli addDenuncia(Denuncia denuncia) {
        this.denuncias.add(denuncia);
        denuncia.setInfosoli(this);
        return this;
    }

    public Infosoli removeDenuncia(Denuncia denuncia) {
        this.denuncias.remove(denuncia);
        denuncia.setInfosoli(null);
        return this;
    }

    public void setDenuncias(Set<Denuncia> denuncias) {
        this.denuncias = denuncias;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Infosoli infosoli = (Infosoli) o;
        if (infosoli.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), infosoli.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Infosoli{" +
            "id=" + getId() +
            ", vInfosoli='" + getvInfosoli() + "'" +
            ", tFecsoli='" + gettFecsoli() + "'" +
            ", vRespuesta='" + getvRespuesta() + "'" +
            ", tFecresp='" + gettFecresp() + "'" +
            ", vUsuareg='" + getvUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", vUsuaupd='" + getvUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
