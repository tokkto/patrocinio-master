package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tipdiligenc.
 */
@Entity
@Table(name = "pjtbc_tipdiligenc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjtbc_tipdiligenc")
public class Tipdiligenc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codtipdil", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 3)
    @Column(name = "v_codsis", length = 3, nullable = false)
    private String vCodsis;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "tipdiligenc")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Tipaudi> tipaudis = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "n_codtipres")
    private Tipresoluc tipresoluc;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodsis() {
        return vCodsis;
    }

    public Tipdiligenc vCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
        return this;
    }

    public void setvCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Tipdiligenc nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Tipdiligenc tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Tipdiligenc nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Tipdiligenc nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Tipdiligenc nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Tipdiligenc tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Tipdiligenc nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Tipaudi> getTipaudis() {
        return tipaudis;
    }

    public Tipdiligenc tipaudis(Set<Tipaudi> tipaudis) {
        this.tipaudis = tipaudis;
        return this;
    }

    public Tipdiligenc addTipaudi(Tipaudi tipaudi) {
        this.tipaudis.add(tipaudi);
        tipaudi.setTipdiligenc(this);
        return this;
    }

    public Tipdiligenc removeTipaudi(Tipaudi tipaudi) {
        this.tipaudis.remove(tipaudi);
        tipaudi.setTipdiligenc(null);
        return this;
    }

    public void setTipaudis(Set<Tipaudi> tipaudis) {
        this.tipaudis = tipaudis;
    }

    public Tipresoluc getTipresoluc() {
        return tipresoluc;
    }

    public Tipdiligenc tipresoluc(Tipresoluc tipresoluc) {
        this.tipresoluc = tipresoluc;
        return this;
    }

    public void setTipresoluc(Tipresoluc tipresoluc) {
        this.tipresoluc = tipresoluc;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tipdiligenc tipdiligenc = (Tipdiligenc) o;
        if (tipdiligenc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipdiligenc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tipdiligenc{" +
            "id=" + getId() +
            ", vCodsis='" + getvCodsis() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
