package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Abogado.
 */
@Entity
@Table(name = "abogado")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "abogado")
public class Abogado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 250)
    @Column(name = "v_nomabogad", length = 250)
    private String vNomabogad;

    @Column(name = "n_codabousu")
    private Integer nCodabousu;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    private Oficina oficina;

    @OneToMany(mappedBy = "abogado")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Legajoasig> legajoasigs = new HashSet<>();

    @OneToMany(mappedBy = "abogado")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Legtipdoc> legtipdocs = new HashSet<>();

    @OneToMany(mappedBy = "abogado")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Falsoexp> falsoexps = new HashSet<>();

    @OneToMany(mappedBy = "abogado")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Concilia> concilias = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvNomabogad() {
        return vNomabogad;
    }

    public Abogado vNomabogad(String vNomabogad) {
        this.vNomabogad = vNomabogad;
        return this;
    }

    public void setvNomabogad(String vNomabogad) {
        this.vNomabogad = vNomabogad;
    }

    public Integer getnCodabousu() {
        return nCodabousu;
    }

    public Abogado nCodabousu(Integer nCodabousu) {
        this.nCodabousu = nCodabousu;
        return this;
    }

    public void setnCodabousu(Integer nCodabousu) {
        this.nCodabousu = nCodabousu;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Abogado nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Abogado tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Abogado nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Abogado nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Abogado nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Abogado tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Abogado nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public Abogado oficina(Oficina oficina) {
        this.oficina = oficina;
        return this;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public Set<Legajoasig> getLegajoasigs() {
        return legajoasigs;
    }

    public Abogado legajoasigs(Set<Legajoasig> legajoasigs) {
        this.legajoasigs = legajoasigs;
        return this;
    }

    public Abogado addLegajoasig(Legajoasig legajoasig) {
        this.legajoasigs.add(legajoasig);
        legajoasig.setAbogado(this);
        return this;
    }

    public Abogado removeLegajoasig(Legajoasig legajoasig) {
        this.legajoasigs.remove(legajoasig);
        legajoasig.setAbogado(null);
        return this;
    }

    public void setLegajoasigs(Set<Legajoasig> legajoasigs) {
        this.legajoasigs = legajoasigs;
    }

    public Set<Legtipdoc> getLegtipdocs() {
        return legtipdocs;
    }

    public Abogado legtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
        return this;
    }

    public Abogado addLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.add(legtipdoc);
        legtipdoc.setAbogado(this);
        return this;
    }

    public Abogado removeLegtipdoc(Legtipdoc legtipdoc) {
        this.legtipdocs.remove(legtipdoc);
        legtipdoc.setAbogado(null);
        return this;
    }

    public void setLegtipdocs(Set<Legtipdoc> legtipdocs) {
        this.legtipdocs = legtipdocs;
    }

    public Set<Falsoexp> getFalsoexps() {
        return falsoexps;
    }

    public Abogado falsoexps(Set<Falsoexp> falsoexps) {
        this.falsoexps = falsoexps;
        return this;
    }

    public Abogado addFalsoexp(Falsoexp falsoexp) {
        this.falsoexps.add(falsoexp);
        falsoexp.setAbogado(this);
        return this;
    }

    public Abogado removeFalsoexp(Falsoexp falsoexp) {
        this.falsoexps.remove(falsoexp);
        falsoexp.setAbogado(null);
        return this;
    }

    public void setFalsoexps(Set<Falsoexp> falsoexps) {
        this.falsoexps = falsoexps;
    }

    public Set<Concilia> getConcilias() {
        return concilias;
    }

    public Abogado concilias(Set<Concilia> concilias) {
        this.concilias = concilias;
        return this;
    }

    public Abogado addConcilia(Concilia concilia) {
        this.concilias.add(concilia);
        concilia.setAbogado(this);
        return this;
    }

    public Abogado removeConcilia(Concilia concilia) {
        this.concilias.remove(concilia);
        concilia.setAbogado(null);
        return this;
    }

    public void setConcilias(Set<Concilia> concilias) {
        this.concilias = concilias;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Abogado abogado = (Abogado) o;
        if (abogado.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), abogado.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Abogado{" +
            "id=" + getId() +
            ", vNomabogad='" + getvNomabogad() + "'" +
            ", nCodabousu='" + getnCodabousu() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
