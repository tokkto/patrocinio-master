package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tipvia.
 */
@Entity
@Table(name = "tipvia")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tipvia")
public class Tipvia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_descrip", length = 200, nullable = false)
    private String vDescrip;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_desccorta", length = 50, nullable = false)
    private String vDesccorta;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_usuareg", length = 20, nullable = false)
    private String vUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @Column(name = "n_flgactivo")
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Size(max = 20)
    @Column(name = "v_usuaupd", length = 20)
    private String vUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "tipvia")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Dirdenun> dirdenuns = new HashSet<>();

    @OneToMany(mappedBy = "tipvia")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Denuncia> denuncias = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvDescrip() {
        return vDescrip;
    }

    public Tipvia vDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
        return this;
    }

    public void setvDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
    }

    public String getvDesccorta() {
        return vDesccorta;
    }

    public Tipvia vDesccorta(String vDesccorta) {
        this.vDesccorta = vDesccorta;
        return this;
    }

    public void setvDesccorta(String vDesccorta) {
        this.vDesccorta = vDesccorta;
    }

    public String getvUsuareg() {
        return vUsuareg;
    }

    public Tipvia vUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
        return this;
    }

    public void setvUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Tipvia tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Tipvia nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Tipvia nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public String getvUsuaupd() {
        return vUsuaupd;
    }

    public Tipvia vUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
        return this;
    }

    public void setvUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Tipvia tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Tipvia nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Dirdenun> getDirdenuns() {
        return dirdenuns;
    }

    public Tipvia dirdenuns(Set<Dirdenun> dirdenuns) {
        this.dirdenuns = dirdenuns;
        return this;
    }

    public Tipvia addDirdenun(Dirdenun dirdenun) {
        this.dirdenuns.add(dirdenun);
        dirdenun.setTipvia(this);
        return this;
    }

    public Tipvia removeDirdenun(Dirdenun dirdenun) {
        this.dirdenuns.remove(dirdenun);
        dirdenun.setTipvia(null);
        return this;
    }

    public void setDirdenuns(Set<Dirdenun> dirdenuns) {
        this.dirdenuns = dirdenuns;
    }

    public Set<Denuncia> getDenuncias() {
        return denuncias;
    }

    public Tipvia denuncias(Set<Denuncia> denuncias) {
        this.denuncias = denuncias;
        return this;
    }

    public Tipvia addDenuncia(Denuncia denuncia) {
        this.denuncias.add(denuncia);
        denuncia.setTipvia(this);
        return this;
    }

    public Tipvia removeDenuncia(Denuncia denuncia) {
        this.denuncias.remove(denuncia);
        denuncia.setTipvia(null);
        return this;
    }

    public void setDenuncias(Set<Denuncia> denuncias) {
        this.denuncias = denuncias;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tipvia tipvia = (Tipvia) o;
        if (tipvia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipvia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tipvia{" +
            "id=" + getId() +
            ", vDescrip='" + getvDescrip() + "'" +
            ", vDesccorta='" + getvDesccorta() + "'" +
            ", vUsuareg='" + getvUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", vUsuaupd='" + getvUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
