package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Dirdenun.
 */
@Entity
@Table(name = "dirdenun")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dirdenun")
public class Dirdenun implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_coddepart", length = 20, nullable = false)
    private String vCoddepart;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_codprovin", length = 20, nullable = false)
    private String vCodprovin;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_coddistri", length = 20, nullable = false)
    private String vCoddistri;

    @NotNull
    @Column(name = "n_codtzona", nullable = false)
    private Integer nCodtzona;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_deszona", length = 200, nullable = false)
    private String vDeszona;

    @NotNull
    @Column(name = "n_codtipvia", nullable = false)
    private Integer nCodtipvia;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_desvia", length = 200, nullable = false)
    private String vDesvia;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_direccion", length = 200, nullable = false)
    private String vDireccion;

    @NotNull
    @Size(max = 200)
    @Column(name = "v_dircomple", length = 200, nullable = false)
    private String vDircomple;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_usuareg", length = 20, nullable = false)
    private String vUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @Column(name = "n_flgactivo")
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Size(max = 20)
    @Column(name = "v_usuaupd", length = 20)
    private String vUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    private Denunte denunte;

    @ManyToOne
    private Tipzona tipzona;

    @ManyToOne
    private Tipvia tipvia;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCoddepart() {
        return vCoddepart;
    }

    public Dirdenun vCoddepart(String vCoddepart) {
        this.vCoddepart = vCoddepart;
        return this;
    }

    public void setvCoddepart(String vCoddepart) {
        this.vCoddepart = vCoddepart;
    }

    public String getvCodprovin() {
        return vCodprovin;
    }

    public Dirdenun vCodprovin(String vCodprovin) {
        this.vCodprovin = vCodprovin;
        return this;
    }

    public void setvCodprovin(String vCodprovin) {
        this.vCodprovin = vCodprovin;
    }

    public String getvCoddistri() {
        return vCoddistri;
    }

    public Dirdenun vCoddistri(String vCoddistri) {
        this.vCoddistri = vCoddistri;
        return this;
    }

    public void setvCoddistri(String vCoddistri) {
        this.vCoddistri = vCoddistri;
    }

    public Integer getnCodtzona() {
        return nCodtzona;
    }

    public Dirdenun nCodtzona(Integer nCodtzona) {
        this.nCodtzona = nCodtzona;
        return this;
    }

    public void setnCodtzona(Integer nCodtzona) {
        this.nCodtzona = nCodtzona;
    }

    public String getvDeszona() {
        return vDeszona;
    }

    public Dirdenun vDeszona(String vDeszona) {
        this.vDeszona = vDeszona;
        return this;
    }

    public void setvDeszona(String vDeszona) {
        this.vDeszona = vDeszona;
    }

    public Integer getnCodtipvia() {
        return nCodtipvia;
    }

    public Dirdenun nCodtipvia(Integer nCodtipvia) {
        this.nCodtipvia = nCodtipvia;
        return this;
    }

    public void setnCodtipvia(Integer nCodtipvia) {
        this.nCodtipvia = nCodtipvia;
    }

    public String getvDesvia() {
        return vDesvia;
    }

    public Dirdenun vDesvia(String vDesvia) {
        this.vDesvia = vDesvia;
        return this;
    }

    public void setvDesvia(String vDesvia) {
        this.vDesvia = vDesvia;
    }

    public String getvDireccion() {
        return vDireccion;
    }

    public Dirdenun vDireccion(String vDireccion) {
        this.vDireccion = vDireccion;
        return this;
    }

    public void setvDireccion(String vDireccion) {
        this.vDireccion = vDireccion;
    }

    public String getvDircomple() {
        return vDircomple;
    }

    public Dirdenun vDircomple(String vDircomple) {
        this.vDircomple = vDircomple;
        return this;
    }

    public void setvDircomple(String vDircomple) {
        this.vDircomple = vDircomple;
    }

    public String getvUsuareg() {
        return vUsuareg;
    }

    public Dirdenun vUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
        return this;
    }

    public void setvUsuareg(String vUsuareg) {
        this.vUsuareg = vUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Dirdenun tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Dirdenun nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Dirdenun nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public String getvUsuaupd() {
        return vUsuaupd;
    }

    public Dirdenun vUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
        return this;
    }

    public void setvUsuaupd(String vUsuaupd) {
        this.vUsuaupd = vUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Dirdenun tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Dirdenun nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Denunte getDenunte() {
        return denunte;
    }

    public Dirdenun denunte(Denunte denunte) {
        this.denunte = denunte;
        return this;
    }

    public void setDenunte(Denunte denunte) {
        this.denunte = denunte;
    }

    public Tipzona getTipzona() {
        return tipzona;
    }

    public Dirdenun tipzona(Tipzona tipzona) {
        this.tipzona = tipzona;
        return this;
    }

    public void setTipzona(Tipzona tipzona) {
        this.tipzona = tipzona;
    }

    public Tipvia getTipvia() {
        return tipvia;
    }

    public Dirdenun tipvia(Tipvia tipvia) {
        this.tipvia = tipvia;
        return this;
    }

    public void setTipvia(Tipvia tipvia) {
        this.tipvia = tipvia;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dirdenun dirdenun = (Dirdenun) o;
        if (dirdenun.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dirdenun.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Dirdenun{" +
            "id=" + getId() +
            ", vCoddepart='" + getvCoddepart() + "'" +
            ", vCodprovin='" + getvCodprovin() + "'" +
            ", vCoddistri='" + getvCoddistri() + "'" +
            ", nCodtzona='" + getnCodtzona() + "'" +
            ", vDeszona='" + getvDeszona() + "'" +
            ", nCodtipvia='" + getnCodtipvia() + "'" +
            ", vDesvia='" + getvDesvia() + "'" +
            ", vDireccion='" + getvDireccion() + "'" +
            ", vDircomple='" + getvDircomple() + "'" +
            ", vUsuareg='" + getvUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", vUsuaupd='" + getvUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
