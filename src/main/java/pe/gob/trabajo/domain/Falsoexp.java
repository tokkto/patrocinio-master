package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Falsoexp.
 */
@Entity
@Table(name = "pjmvc_falsoexp")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjmvc_falsoexp")
public class Falsoexp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codfalexp", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 5)
    @Column(name = "v_codreg", length = 5, nullable = false)
    private String vCodreg;

    @NotNull
    @Size(max = 10)
    @Column(name = "v_cargofal", length = 10, nullable = false)
    private String vCargofal;

    @NotNull
    @Size(max = 50)
    @Column(name = "v_demndte", length = 50, nullable = false)
    private String vDemndte;

    @NotNull
    @Size(max = 150)
    @Column(name = "v_demandado", length = 150, nullable = false)
    private String vDemandado;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_dnidmdo", length = 15, nullable = false)
    private String vDnidmdo;

    @NotNull
    @Size(max = 10)
    @Column(name = "v_dnidmte", length = 10, nullable = false)
    private String vDnidmte;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_nombres", length = 100, nullable = false)
    private String vNombres;

    @NotNull
    @Size(max = 4)
    @Column(name = "v_diffal", length = 4, nullable = false)
    private String vDiffal;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codemplea")
    private Empleador empleador;

    @ManyToOne
    @JoinColumn(name = "n_codtrab")
    private Trabajador trabajador;

    @ManyToOne
    @JoinColumn(name = "n_codtabogad")
    private Abogado abogado;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodreg() {
        return vCodreg;
    }

    public Falsoexp vCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
        return this;
    }

    public void setvCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getvCargofal() {
        return vCargofal;
    }

    public Falsoexp vCargofal(String vCargofal) {
        this.vCargofal = vCargofal;
        return this;
    }

    public void setvCargofal(String vCargofal) {
        this.vCargofal = vCargofal;
    }

    public String getvDemndte() {
        return vDemndte;
    }

    public Falsoexp vDemndte(String vDemndte) {
        this.vDemndte = vDemndte;
        return this;
    }

    public void setvDemndte(String vDemndte) {
        this.vDemndte = vDemndte;
    }

    public String getvDemandado() {
        return vDemandado;
    }

    public Falsoexp vDemandado(String vDemandado) {
        this.vDemandado = vDemandado;
        return this;
    }

    public void setvDemandado(String vDemandado) {
        this.vDemandado = vDemandado;
    }

    public String getvDnidmdo() {
        return vDnidmdo;
    }

    public Falsoexp vDnidmdo(String vDnidmdo) {
        this.vDnidmdo = vDnidmdo;
        return this;
    }

    public void setvDnidmdo(String vDnidmdo) {
        this.vDnidmdo = vDnidmdo;
    }

    public String getvDnidmte() {
        return vDnidmte;
    }

    public Falsoexp vDnidmte(String vDnidmte) {
        this.vDnidmte = vDnidmte;
        return this;
    }

    public void setvDnidmte(String vDnidmte) {
        this.vDnidmte = vDnidmte;
    }

    public String getvNombres() {
        return vNombres;
    }

    public Falsoexp vNombres(String vNombres) {
        this.vNombres = vNombres;
        return this;
    }

    public void setvNombres(String vNombres) {
        this.vNombres = vNombres;
    }

    public String getvDiffal() {
        return vDiffal;
    }

    public Falsoexp vDiffal(String vDiffal) {
        this.vDiffal = vDiffal;
        return this;
    }

    public void setvDiffal(String vDiffal) {
        this.vDiffal = vDiffal;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Falsoexp nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Falsoexp tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Falsoexp nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Falsoexp nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Falsoexp nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Falsoexp tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Falsoexp nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Empleador getEmpleador() {
        return empleador;
    }

    public Falsoexp empleador(Empleador empleador) {
        this.empleador = empleador;
        return this;
    }

    public void setEmpleador(Empleador empleador) {
        this.empleador = empleador;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public Falsoexp trabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
        return this;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Abogado getAbogado() {
        return abogado;
    }

    public Falsoexp abogado(Abogado abogado) {
        this.abogado = abogado;
        return this;
    }

    public void setAbogado(Abogado abogado) {
        this.abogado = abogado;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Falsoexp falsoexp = (Falsoexp) o;
        if (falsoexp.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), falsoexp.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Falsoexp{" +
            "id=" + getId() +
            ", vCodreg='" + getvCodreg() + "'" +
            ", vCargofal='" + getvCargofal() + "'" +
            ", vDemndte='" + getvDemndte() + "'" +
            ", vDemandado='" + getvDemandado() + "'" +
            ", vDnidmdo='" + getvDnidmdo() + "'" +
            ", vDnidmte='" + getvDnidmte() + "'" +
            ", vNombres='" + getvNombres() + "'" +
            ", vDiffal='" + getvDiffal() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
