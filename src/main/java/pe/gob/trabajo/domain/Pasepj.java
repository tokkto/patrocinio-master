package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Pasepj.
 */
@Entity
@Table(name = "pjmvc_pasepj")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pjmvc_pasepj")
public class Pasepj implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codpasepj", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codreg", length = 2, nullable = false)
    private String vCodreg;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codzon", length = 2, nullable = false)
    private String vCodzon;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codtdcide", length = 2, nullable = false)
    private String vCodtdcide;

    @NotNull
    @Column(name = "n_correl", nullable = false)
    private Integer nCorrel;

    @NotNull
    @Size(max = 3)
    @Column(name = "v_codsis", length = 3, nullable = false)
    private String vCodsis;

    @NotNull
    @Column(name = "n_codsuc", nullable = false)
    private Integer nCodsuc;

    @NotNull
    @Size(max = 10)
    @Column(name = "v_codcon", length = 10, nullable = false)
    private String vCodcon;

    @Column(name = "d_fecpas")
    private LocalDate dFecpas;

    @Column(name = "d_fecrecep")
    private LocalDate dFecrecep;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusurec", length = 15, nullable = false)
    private String vCodusurec;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusureg", length = 15, nullable = false)
    private String vCodusureg;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostreg", length = 30, nullable = false)
    private String vHostreg;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_codusumod", length = 15, nullable = false)
    private String vCodusumod;

    @Column(name = "d_fecmod")
    private LocalDate dFecmod;

    @NotNull
    @Size(max = 30)
    @Column(name = "v_hostmod", length = 30, nullable = false)
    private String vHostmod;

    @NotNull
    @Size(max = 3)
    @Column(name = "v_codsisdes", length = 3, nullable = false)
    private String vCodsisdes;

    @Column(name = "d_fecdes")
    private LocalDate dFecdes;

    @Column(name = "d_feccon")
    private LocalDate dFeccon;

    @NotNull
    @Size(max = 11)
    @Column(name = "v_codtrar", length = 11, nullable = false)
    private String vCodtrar;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codtdcidr", length = 2, nullable = false)
    private String vCodtdcidr;

    @NotNull
    @Column(name = "n_correlr", nullable = false)
    private Integer nCorrelr;

    @NotNull
    @Size(max = 500)
    @Column(name = "v_obspas", length = 500, nullable = false)
    private String vObspas;

    @NotNull
    @Size(max = 11)
    @Column(name = "v_codtrac", length = 11, nullable = false)
    private String vCodtrac;

    @NotNull
    @Size(max = 2)
    @Column(name = "v_codtdcidi", length = 2, nullable = false)
    private String vCodtdcidi;

    @NotNull
    @Column(name = "n_correlc", nullable = false)
    private Integer nCorrelc;

    @NotNull
    @Size(max = 6)
    @Column(name = "v_codloc", length = 6, nullable = false)
    private String vCodloc;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codemplea")
    private Empleador empleador;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvCodreg() {
        return vCodreg;
    }

    public Pasepj vCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
        return this;
    }

    public void setvCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getvCodzon() {
        return vCodzon;
    }

    public Pasepj vCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
        return this;
    }

    public void setvCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
    }

    public String getvCodtdcide() {
        return vCodtdcide;
    }

    public Pasepj vCodtdcide(String vCodtdcide) {
        this.vCodtdcide = vCodtdcide;
        return this;
    }

    public void setvCodtdcide(String vCodtdcide) {
        this.vCodtdcide = vCodtdcide;
    }

    public Integer getnCorrel() {
        return nCorrel;
    }

    public Pasepj nCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
        return this;
    }

    public void setnCorrel(Integer nCorrel) {
        this.nCorrel = nCorrel;
    }

    public String getvCodsis() {
        return vCodsis;
    }

    public Pasepj vCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
        return this;
    }

    public void setvCodsis(String vCodsis) {
        this.vCodsis = vCodsis;
    }

    public Integer getnCodsuc() {
        return nCodsuc;
    }

    public Pasepj nCodsuc(Integer nCodsuc) {
        this.nCodsuc = nCodsuc;
        return this;
    }

    public void setnCodsuc(Integer nCodsuc) {
        this.nCodsuc = nCodsuc;
    }

    public String getvCodcon() {
        return vCodcon;
    }

    public Pasepj vCodcon(String vCodcon) {
        this.vCodcon = vCodcon;
        return this;
    }

    public void setvCodcon(String vCodcon) {
        this.vCodcon = vCodcon;
    }

    public LocalDate getdFecpas() {
        return dFecpas;
    }

    public Pasepj dFecpas(LocalDate dFecpas) {
        this.dFecpas = dFecpas;
        return this;
    }

    public void setdFecpas(LocalDate dFecpas) {
        this.dFecpas = dFecpas;
    }

    public LocalDate getdFecrecep() {
        return dFecrecep;
    }

    public Pasepj dFecrecep(LocalDate dFecrecep) {
        this.dFecrecep = dFecrecep;
        return this;
    }

    public void setdFecrecep(LocalDate dFecrecep) {
        this.dFecrecep = dFecrecep;
    }

    public String getvCodusurec() {
        return vCodusurec;
    }

    public Pasepj vCodusurec(String vCodusurec) {
        this.vCodusurec = vCodusurec;
        return this;
    }

    public void setvCodusurec(String vCodusurec) {
        this.vCodusurec = vCodusurec;
    }

    public String getvCodusureg() {
        return vCodusureg;
    }

    public Pasepj vCodusureg(String vCodusureg) {
        this.vCodusureg = vCodusureg;
        return this;
    }

    public void setvCodusureg(String vCodusureg) {
        this.vCodusureg = vCodusureg;
    }

    public String getvHostreg() {
        return vHostreg;
    }

    public Pasepj vHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
        return this;
    }

    public void setvHostreg(String vHostreg) {
        this.vHostreg = vHostreg;
    }

    public String getvCodusumod() {
        return vCodusumod;
    }

    public Pasepj vCodusumod(String vCodusumod) {
        this.vCodusumod = vCodusumod;
        return this;
    }

    public void setvCodusumod(String vCodusumod) {
        this.vCodusumod = vCodusumod;
    }

    public LocalDate getdFecmod() {
        return dFecmod;
    }

    public Pasepj dFecmod(LocalDate dFecmod) {
        this.dFecmod = dFecmod;
        return this;
    }

    public void setdFecmod(LocalDate dFecmod) {
        this.dFecmod = dFecmod;
    }

    public String getvHostmod() {
        return vHostmod;
    }

    public Pasepj vHostmod(String vHostmod) {
        this.vHostmod = vHostmod;
        return this;
    }

    public void setvHostmod(String vHostmod) {
        this.vHostmod = vHostmod;
    }

    public String getvCodsisdes() {
        return vCodsisdes;
    }

    public Pasepj vCodsisdes(String vCodsisdes) {
        this.vCodsisdes = vCodsisdes;
        return this;
    }

    public void setvCodsisdes(String vCodsisdes) {
        this.vCodsisdes = vCodsisdes;
    }

    public LocalDate getdFecdes() {
        return dFecdes;
    }

    public Pasepj dFecdes(LocalDate dFecdes) {
        this.dFecdes = dFecdes;
        return this;
    }

    public void setdFecdes(LocalDate dFecdes) {
        this.dFecdes = dFecdes;
    }

    public LocalDate getdFeccon() {
        return dFeccon;
    }

    public Pasepj dFeccon(LocalDate dFeccon) {
        this.dFeccon = dFeccon;
        return this;
    }

    public void setdFeccon(LocalDate dFeccon) {
        this.dFeccon = dFeccon;
    }

    public String getvCodtrar() {
        return vCodtrar;
    }

    public Pasepj vCodtrar(String vCodtrar) {
        this.vCodtrar = vCodtrar;
        return this;
    }

    public void setvCodtrar(String vCodtrar) {
        this.vCodtrar = vCodtrar;
    }

    public String getvCodtdcidr() {
        return vCodtdcidr;
    }

    public Pasepj vCodtdcidr(String vCodtdcidr) {
        this.vCodtdcidr = vCodtdcidr;
        return this;
    }

    public void setvCodtdcidr(String vCodtdcidr) {
        this.vCodtdcidr = vCodtdcidr;
    }

    public Integer getnCorrelr() {
        return nCorrelr;
    }

    public Pasepj nCorrelr(Integer nCorrelr) {
        this.nCorrelr = nCorrelr;
        return this;
    }

    public void setnCorrelr(Integer nCorrelr) {
        this.nCorrelr = nCorrelr;
    }

    public String getvObspas() {
        return vObspas;
    }

    public Pasepj vObspas(String vObspas) {
        this.vObspas = vObspas;
        return this;
    }

    public void setvObspas(String vObspas) {
        this.vObspas = vObspas;
    }

    public String getvCodtrac() {
        return vCodtrac;
    }

    public Pasepj vCodtrac(String vCodtrac) {
        this.vCodtrac = vCodtrac;
        return this;
    }

    public void setvCodtrac(String vCodtrac) {
        this.vCodtrac = vCodtrac;
    }

    public String getvCodtdcidi() {
        return vCodtdcidi;
    }

    public Pasepj vCodtdcidi(String vCodtdcidi) {
        this.vCodtdcidi = vCodtdcidi;
        return this;
    }

    public void setvCodtdcidi(String vCodtdcidi) {
        this.vCodtdcidi = vCodtdcidi;
    }

    public Integer getnCorrelc() {
        return nCorrelc;
    }

    public Pasepj nCorrelc(Integer nCorrelc) {
        this.nCorrelc = nCorrelc;
        return this;
    }

    public void setnCorrelc(Integer nCorrelc) {
        this.nCorrelc = nCorrelc;
    }

    public String getvCodloc() {
        return vCodloc;
    }

    public Pasepj vCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
        return this;
    }

    public void setvCodloc(String vCodloc) {
        this.vCodloc = vCodloc;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Pasepj nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Pasepj tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Pasepj nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Pasepj nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Pasepj nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Pasepj tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Pasepj nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Empleador getEmpleador() {
        return empleador;
    }

    public Pasepj empleador(Empleador empleador) {
        this.empleador = empleador;
        return this;
    }

    public void setEmpleador(Empleador empleador) {
        this.empleador = empleador;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pasepj pasepj = (Pasepj) o;
        if (pasepj.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pasepj.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pasepj{" +
            "id=" + getId() +
            ", vCodreg='" + getvCodreg() + "'" +
            ", vCodzon='" + getvCodzon() + "'" +
            ", vCodtdcide='" + getvCodtdcide() + "'" +
            ", nCorrel='" + getnCorrel() + "'" +
            ", vCodsis='" + getvCodsis() + "'" +
            ", nCodsuc='" + getnCodsuc() + "'" +
            ", vCodcon='" + getvCodcon() + "'" +
            ", dFecpas='" + getdFecpas() + "'" +
            ", dFecrecep='" + getdFecrecep() + "'" +
            ", vCodusurec='" + getvCodusurec() + "'" +
            ", vCodusureg='" + getvCodusureg() + "'" +
            ", vHostreg='" + getvHostreg() + "'" +
            ", vCodusumod='" + getvCodusumod() + "'" +
            ", dFecmod='" + getdFecmod() + "'" +
            ", vHostmod='" + getvHostmod() + "'" +
            ", vCodsisdes='" + getvCodsisdes() + "'" +
            ", dFecdes='" + getdFecdes() + "'" +
            ", dFeccon='" + getdFeccon() + "'" +
            ", vCodtrar='" + getvCodtrar() + "'" +
            ", vCodtdcidr='" + getvCodtdcidr() + "'" +
            ", nCorrelr='" + getnCorrelr() + "'" +
            ", vObspas='" + getvObspas() + "'" +
            ", vCodtrac='" + getvCodtrac() + "'" +
            ", vCodtdcidi='" + getvCodtdcidi() + "'" +
            ", nCorrelc='" + getnCorrelc() + "'" +
            ", vCodloc='" + getvCodloc() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
